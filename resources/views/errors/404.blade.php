<!DOCTYPE html>
<html lang="en">

<head>
    <!--- Basic Page Needs  -->
    <meta charset="utf-8">
    <title>404 || {{env('APP_NAME','Localhost')}}</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keywords" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Mobile Specific Meta  -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    @include('frontend._includes.styles')
    <link rel="shortcut icon" type="{{asset('frontend/image/png')}}" href="img/favicon.ico">
</head>

<body data-spy="scroll" data-target="#scroll-menu" data-offset="100">
    <!-- <div id="preloader"></div> -->
    
    <!-- other-top-pages-start -->
    <div class="other-pages-top-area bg-with-black">
        <div class="container">
            <div class="row">
                <div class="col-col-xs-12">
                    <div class="other-pages">
                        <div class="clearfix">
                            <h2 class="op-title">404 PAGE</h2>
                            <p class="op-text">Until i discovered cooking i was never really interested in anything</p>
                            <!-- <ul class="op-list">
                                <li><a href="index.html">Home</a></li>
                                <li>404 Page</li>
                            </ul> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- other-top-pages-end -->
    <!-- error-area-start -->
    <div class="error-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="errors">
                        <h2 class="errors-title">Ooops! Something went wrong! Page Not Found</h2>
                        <p class="errors-text">If the page is not found, return to the <a href="{{url()->previous()}}">Previous Page</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- error-area-end -->
    
    @include('frontend._components.footer')

    @include('frontend._includes.scripts')
</body>

</html>
