<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableWebInfoAddColumnGeoLatAndGeoLong extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('web_info', function($table)
        {
            $table->text('geo_lat')->nullable()->after('address');
            $table->text('geo_long')->nullable()->after('geo_lat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('web_info', function($table)
        {
            $table->dropColumn(array('geo_lat','geo_long'));
        });
    }
}
