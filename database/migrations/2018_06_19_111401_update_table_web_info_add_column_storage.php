<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableWebInfoAddColumnStorage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('web_info', function($table)
        {

            $table->text('description')->nullable()->after('address');
            $table->text('path')->nullable()->after('description');
            $table->text('directory')->nullable()->after('path');
            $table->text('filename')->nullable()->after('directory');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('web_info', function($table)
        {
            $table->dropColumn(array('description','path','directory','filename'));
        });
    }
}
