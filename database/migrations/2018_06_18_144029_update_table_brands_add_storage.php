<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableBrandsAddStorage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('brands', function($table)
        {
            $table->text('path_logo')->nullable()->after('filename');
            $table->text('directory_logo')->nullable()->after('path_logo');
            $table->text('filename_logo')->nullable()->after('directory_logo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('brands', function($table)
        {
            $table->dropColumn(array('path_logo','directory_logo','filename_logo'));
        });
    }
}
