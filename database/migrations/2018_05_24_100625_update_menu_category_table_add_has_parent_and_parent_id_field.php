<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateMenuCategoryTableAddHasParentAndParentIdField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('menu_category', function($table)
        {
            $table->enum("has_parent",['yes','no'])->default('no')->after('display_sequence');
            $table->integer("parent_id")->default(0)->after('display_sequence');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('menu_category', function($table)
        {
            $table->dropColumn(array('has_parent','parent_id'));
        });
    }
}
