<?php


/**
 *
 * ------------------------------------
 * Backoffice Routes
 * ------------------------------------
 *
 */

Route::group(

	array(
		'as' => "backoffice.",
		'prefix' => "admin",
		'namespace' => "Backoffice",
	),

	function() {

		$this->group(['as'=>"auth.", 'middleware' => ["web","backoffice.guest"]], function(){
			$this->get('login',['as' => "login",'uses' => "AuthController@login"]);
			$this->post('login',['uses' => "AuthController@authenticate"]);
		});

		$this->group(['middleware' => "backoffice.auth"], function(){

			$this->get('/',['as' => "dashboard",'uses' => "DashboardController@index"]);
			$this->get('voters',['as' => "voters",'uses' => "VotersController@index"]);
			$this->get('logout',['as' => "logout",'uses' => "AuthController@destroy"]);


			$this->group(['prefix' => "profile", 'as' => "profile."], function () {
				$this->get('/',['as' => "settings", 'uses' => "ProfileController@setting"]);
				$this->post('/',['as' => "update_setting",'uses' => "ProfileController@update_setting"]);
				$this->post('update-password',['as' => "update_password",'uses' => "ProfileController@update_password"]);
			});

			$this->group(['prefix' => "contact-inquiries", 'as' => "contact_inquiry."], function () {
				$this->get('/',['as' => "index", 'uses' => "ContactInquiryController@index"]);
			});

			$this->group(['prefix' => "menu-category", 'as' => "menu_category."], function () {
				$this->get('/',['as' => "index", 'uses' => "MenuCategoryController@index"]);
				$this->post('create',['as' => "store",'uses' => "MenuCategoryController@store"]);
				$this->post('edit/{id?}',['as' => "update",'uses' => "MenuCategoryController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "MenuCategoryController@destroy"]);
			});

			$this->group(['prefix' => "sliders", 'as' => "sliders."], function () {
				$this->get('/',['as' => "index", 'uses' => "SlidersController@index"]);
				$this->get('create',['as' => "create",'uses' => "SlidersController@create"]);
				$this->post('create',['uses' => "SlidersController@store"]);
				$this->get('edit/{id?}',['as' => "edit",'uses' => "SlidersController@edit"]);
				$this->post('edit/{id?}',['uses' => "SlidersController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "SlidersController@destroy"]);
			});

			$this->group(['prefix' => "sub-content", 'as' => "sub_content."], function () {
				$this->get('/',['as' => "index", 'uses' => "SubContentController@index"]);
				$this->get('create',['as' => "create",'uses' => "SubContentController@create"]);
				$this->post('create',['uses' => "SubContentController@store"]);
				$this->get('edit/{id?}',['as' => "edit",'uses' => "SubContentController@edit"]);
				$this->post('edit/{id?}',['uses' => "SubContentController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "SubContentController@destroy"]);
			});

			$this->group(['prefix' => "brands", 'as' => "brands."], function () {
				$this->get('/',['as' => "index", 'uses' => "BrandsController@index"]);
				$this->get('create',['as' => "create",'uses' => "BrandsController@create"]);
				$this->post('create',['uses' => "BrandsController@store"]);
				$this->get('edit/{id?}',['as' => "edit",'uses' => "BrandsController@edit"]);
				$this->post('edit/{id?}',['uses' => "BrandsController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "BrandsController@destroy"]);
			});
			$this->group(['prefix' => "partners", 'as' => "partners."], function () {
				$this->get('/',['as' => "index", 'uses' => "PartnersController@index"]);
				$this->get('create',['as' => "create",'uses' => "PartnersController@create"]);
				$this->post('create',['uses' => "PartnersController@store"]);
				$this->get('edit/{id?}',['as' => "edit",'uses' => "PartnersController@edit"]);
				$this->post('edit/{id?}',['uses' => "PartnersController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "PartnersController@destroy"]);
			});

			$this->group(['prefix' => "team", 'as' => "team."], function () {
				$this->get('/',['as' => "index", 'uses' => "TeamController@index"]);
				$this->get('create',['as' => "create",'uses' => "TeamController@create"]);
				$this->post('create',['uses' => "TeamController@store"]);
				$this->get('edit/{id?}',['as' => "edit",'uses' => "TeamController@edit"]);
				$this->post('edit/{id?}',['uses' => "TeamController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "TeamController@destroy"]);
			});

			$this->group(['prefix' => "career", 'as' => "career."], function () {
				$this->get('/',['as' => "index", 'uses' => "CareerController@index"]);
				$this->get('create',['as' => "create",'uses' => "CareerController@create"]);
				$this->post('create',['uses' => "CareerController@store"]);
				$this->get('edit/{id?}',['as' => "edit",'uses' => "CareerController@edit"]);
				$this->post('edit/{id?}',['uses' => "CareerController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "CareerController@destroy"]);
			});

			$this->group(['prefix' => "web_info", 'as' => "web_info."], function () {
				$this->get('/',['as' => "index", 'uses' => "WebInfoController@index"]);
				$this->post('/',['as' => "index", 'uses' => "WebInfoController@store"]);
				// $this->post('create',['uses' => "WebInfoController@store"]);
				// $this->get('edit/{id?}',['as' => "edit",'uses' => "WebInfoController@edit"]);
				// $this->post('edit/{id?}',['uses' => "WebInfoController@update"]);
				// $this->any('delete/{id?}',['as' => "destroy", 'uses' => "WebInfoController@destroy"]);
			});

			
			$this->group(['prefix' => "social_links", 'as' => "social_links."], function () {
				$this->get('/',['as' => "index", 'uses' => "SocialLinkController@index"]);
				$this->get('create',['as' => "create",'uses' => "SocialLinkController@create"]);
				$this->post('create',['uses' => "SocialLinkController@store"]);
				$this->get('edit/{id?}',['as' => "edit",'uses' => "SocialLinkController@edit"]);
				$this->post('edit/{id?}',['uses' => "SocialLinkController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "SocialLinkController@destroy"]);
			});

			$this->group(['prefix' => "page_content", 'as' => "page_content."], function () {
				$this->get('/',['as' => "index", 'uses' => "PageContentController@index"]);
				$this->get('create',['as' => "create",'uses' => "PageContentController@create"]);
				$this->post('create',['uses' => "PageContentController@store"]);
				$this->get('edit/{id?}',['as' => "edit",'uses' => "PageContentController@edit"]);
				$this->post('edit/{id?}',['uses' => "PageContentController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "PageContentController@destroy"]);
			});


			

			
			
		});
	}
);