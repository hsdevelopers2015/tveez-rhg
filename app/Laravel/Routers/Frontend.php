<?php


/**
 *
 * ------------------------------------
 * Frontend Routes
 * ------------------------------------
 *
 */

$this->group(

	array(
		'as' => "frontend.",
		'namespace' => "Frontend",
	),

	function() {

		$this->get('/',['as' => "index",'uses' => "PageController@index"]);
		$this->get('brand',['as' => "brand",'uses' => "PageController@brand"]);
		$this->get('partners',['as' => "partners",'uses' => "PageController@partners"]);
		$this->get('contact',['as' => "contact",'uses' => "PageController@contact"]);
		$this->post('send',['as' => "send",'uses' => "PageController@send"]);
		// $this->get('careers',['as' => "careers",'uses' => "PageController@careers"]);

		$this->group(['prefix' => "careers", 'as' => "careers."], function () {
			$this->get('/',['as' => "index", 'uses' => "PageController@careers"]);
			$this->get('show/{id?}',['as' => "show", 'uses' => "PageController@careers_show"]);

		});
		$this->get('about',['as' => "about",'uses' => "PageController@about"]);
		$this->get('footer',['uses' => "PageController@footer"]);

		$this->get('contact',['as' => "contact",'uses' => "PageController@contact"]);
		$this->post('contact',['uses' => "PageController@sent"]);

		$this->get('edit/{id?}',['as' => "edit",'uses' => "BrandsController@edit"]);
		// $this->post('contact',['as' => "sent",'uses' => "PageController@sent"]);
		// $this->get('contact',['as' => "contact",'uses' => "PageController@contact"]);
		// $this->post('subscribe',['as' => "subscribe",'uses' => "PageController@subscribe"]);
		// $this->any('unsubscribe/{email}',['as' => "unsubscribe",'uses' => "PageController@unsubscribe"]);
		// $this->get('{id}/{category}',['as' => "menu_category", 'uses' => "PageController@menu"]);
	}
);