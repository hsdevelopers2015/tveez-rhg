<?php 

namespace App\Laravel\Requests\Backoffice;

use App\Laravel\Requests\RequestManager;

use Auth;

class PartnerRequest extends RequestManager
{
    public function rules() {

        $user = Auth::user();

        $rules = [
            'file'     => "image",
            'link'     => "required",
            'content'     => "required",
        ];


        return $rules;
    }

    public function messages() {
        return [
            'required'  => "Field is required.",
            'old_password' => "Incorrect password.",
        ];
    }
}