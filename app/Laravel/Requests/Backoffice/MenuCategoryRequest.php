<?php 

namespace App\Laravel\Requests\Backoffice;

use App\Laravel\Requests\RequestManager;

use Auth;

class MenuCategoryRequest extends RequestManager
{
    public function rules() {

        $user = Auth::user();

        $rules = [
            'category_name'     => "required",
            'file'     => "image",
        ];


        return $rules;
    }

    public function messages() {
        return [
            'required'  => "Field is required.",
            'old_password' => "Incorrect password.",
        ];
    }
}