<?php 

namespace App\Laravel\Requests\Backoffice;

use App\Laravel\Requests\RequestManager;

use Auth;

class PageContentRequest extends RequestManager
{
    public function rules() {

        $user = Auth::user();

        $rules = [
            'title'     => "required",
            'page_location'     => "required",
            'content'     => "required",
            'file'     => "image",
            'order'     => "",
        ];


        return $rules;
    }

    public function messages() {
        return [
            'required'  => "Field is required.",
            'old_password' => "Incorrect password.",
        ];
    }
}