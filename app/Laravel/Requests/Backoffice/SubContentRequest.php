<?php 

namespace App\Laravel\Requests\Backoffice;

use App\Laravel\Requests\RequestManager;

use Auth;

class SubContentRequest extends RequestManager
{
    public function rules() {

        $user = Auth::user();

        $rules = [
            'page_location'     => "required",
            'content'     => "required",
        ];


        return $rules;
    }

    public function messages() {
        return [
            'required'  => "Field is required.",
            'old_password' => "Incorrect password.",
        ];
    }
}