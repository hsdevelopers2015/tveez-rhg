<?php 

namespace App\Laravel\Requests\Backoffice;

use App\Laravel\Requests\RequestManager;

use Auth;

class WebInfoRequest extends RequestManager
{
    public function rules() {

        $user = Auth::user();

        $rules = [
            'file'     => "image",
            'description'     => "required",
            'phone'     => "required",
            'operations'     => "required",
            'fax'     => "required",
            'email'     => "required",
            // 'address'     => "required",
            'geo_lat' => "required",
            'geo_long' => "required"
        ];


        return $rules;
    }

    public function messages() {
        return [
            'required'  => "Field is required.",
            'old_password' => "Incorrect password.",
        ];
    }
}