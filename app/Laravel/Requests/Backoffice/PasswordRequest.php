<?php

namespace App\Laravel\Requests\Backoffice;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class PasswordRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
	    return true;
	}
	
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */



	public function rules()
	{

		$rules = [
			'old_password' => 'required',
			'new_password' => 'required|confirmed',
			'new_password_confirmation' => 'required',
		];
		return $rules;
	}

	public function messages(){
		return [
			'old_password.old_password' => "Please enter your current password.",
		];
	}
}