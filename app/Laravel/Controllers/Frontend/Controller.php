<?php 

namespace App\Laravel\Controllers\Frontend;

use App\Laravel\Models\User;
use App\Laravel\Models\News;
use App\Laravel\Models\SocialLink;
use App\Laravel\Models\MenuCategory;
use App\Laravel\Models\WebInfo;

use Illuminate\Support\Collection;
use App\Http\Controllers\Controller as MainController;
use Auth, Session,Carbon, Helper,Route,DNS2D;

class Controller extends MainController{

	protected $data;

	public function __construct(){
		self::set_menu_categories();
		self::set_news();
		self::set_social_links();
		self::set_web_info();
	}

	public function get_data(){
		return $this->data;
	}

	public function set_menu_categories(){
		$this->data['categories'] = MenuCategory::where('has_parent','no')->get();
	}

	public function set_news(){
		$this->data['news'] = News::orderBy('publish_at','DESC')->get();
	}

	public function set_social_links(){
		$this->data['social_links'] = SocialLink::all();
	}

	public function set_web_info(){
		return $this->data['web_info'] = WebInfo::orderBy('created_at',"DESC")->first() ? : new WebInfo;
	}
}