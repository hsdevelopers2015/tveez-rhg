<?php 

namespace App\Laravel\Controllers\Frontend;

/*
*
* Models used for this controller
*/
use App\User;
use App\Laravel\Models\Slider;
use App\Laravel\Models\Team;
use App\Laravel\Models\Brand;
use App\Laravel\Models\PageContent;
use App\Laravel\Models\WebInfo;
use App\Laravel\Models\Partner;
use App\Laravel\Models\Career;
use App\Laravel\Models\ContactInquiry;
use App\Laravel\Models\Brands;
use App\Laravel\Models\SubContent;


/*
*
* Requests used for validating inputs
*/

use App\Laravel\Requests\Frontend\ContactInquiryRequest;
use App\Laravel\Events\SendEmail;

/*
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str, DB, Input, Event;

class PageController extends Controller{

	/*
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());

		$this->data['contact'] = PageContent::where('page_location','contact')->first() ? : new PageContent; 
		$this->data['info'] = WebInfo::orderBy('created_at','DESC')->first() ? : new WebInfo;		
		$this->data['brands'] = Brand::orderBy('created_at','ASC')->get() ? : new Brand;
		$this->data['brand_subcontent'] = SubContent::orderBy('created_at','DESC')->first() ? : new SubContent;
	}

	public function index () {

		$this->data['sliders'] = Slider::orderBy('created_at','DESC')->take(5)->get() ? : new Slider;		
		$this->data['team'] = Team::orderBy('created_at','ASC')->take(5)->get() ? : new Team;		
		$this->data['partners'] = Partner::orderBy('created_at','ASC')->get() ? : new Partner;	
		

		return view('frontend.index',$this->data);
	}

	public function brand () {
		return view('frontend.brand',$this->data);
	}

	public function footer () {
		// $this->data['footer'] = WebInfo::orderBy('created_at','DESC')->first();
		$this->data['footer'] ="sdsdsdsd";
		return view('frontend._components.footer',$this->data);
	}

	public function send(ContactInquiryRequest $request){
		$new_contact_inquiry = new ContactInquiry;
		$new_contact_inquiry->fill($request->all());

		// dd($request->all());

		if($new_contact_inquiry->save()){
			Session::flash('notification-status','success');
			Session::flash('notification-msg',"You're inquiry was successfully sent.");

			// $notification_data = new SendEmail(['input' => $request->all(),'type' => "inquiry"]);
			// Event::fire('send_email', $notification_data);

			return redirect()->back();
		}
		return view('frontend.index',$this->data);
	}	

	public function partners () {
		$this->data['partners'] = Partner::orderBy('created_at','DESC')->get() ? : new Partner;
		return view('frontend.partners',$this->data);
	}

	public function contact () {
		$this->data['web_info'] = WebInfo::orderBy('created_at','DESC')->first() ?: new WebInfo;
		

		return view('frontend.contact',$this->data);
	}

	public function careers () {
		$this->data['careers'] = Career::orderBy('created_at','DESC')->get() ? : new Career;		
		$this->data['sub_content'] = SubContent::orderBy('created_at','DESC')->first() ? : new SubContent;
		return view('frontend.career.index',$this->data);
	}

	public function careers_show($id = NULL){
		$this->data['career'] = Career::where('id',$id)->first() ? : new Career;
		return view('frontend.career.show',$this->data);
	}

	public function about () {

		$this->data['description'] = WebInfo::orderBy('created_at','DESC')->first() ?: new WebInfo;
		$this->data['about'] = PageContent::orderBy('created_at','DESC')->where('page_location','about')->first() ?: new PageContent;
		$this->data['milestone'] = PageContent::orderBy('created_at','DESC')->where('page_location','milestone')->first() ?: new PageContent;
		$this->data['mission'] = PageContent::orderBy('created_at','DESC')->where('page_location','mission')->first() ?: new PageContent;
		$this->data['vision'] = PageContent::orderBy('created_at','DESC')->where('page_location','vision')->first() ?: new PageContent;

		$this->data['others'] = PageContent::orderBy('order','ASC')->where('page_location','others')->get() ?: new PageContent;
		$this->data['vision_mission_header'] = $this->data['mission']->title." & ".$this->data['vision']->title ?: "";
		$this->data['team'] = Team::orderBy('created_at','ASC')->take(5)->get() ? : new Team;	

		return view('frontend.about',$this->data);
	}

	public function menu($id = NULL, $category = NULL){
		$this->data['category'] = MenuCategory::find($id);
		$this->data['children'] = MenuCategory::where('parent_id',$id)->where('has_parent','yes')->get();

		if(!$this->data['category']){
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',"Menu not found.");
		}
		// $child_ids = [];
		// foreach($children as $index => $child){
		// 	array_push($child_ids,$child->id);
		// }

		$this->data['meals'] = Meal::where('category_id',$id)->get();
		// $this->data['children'] = Meal::

		return view('frontend.menu',$this->data);
		
	}

	public function sent(ContactInquiryRequest $request){
		$contact = new ContactInquiry;

		$contact->fill(Input::all());

		if($contact->save()){
			return redirect()->intended('/#contact')->with(['notification-status'=>'success', 
						'notification-msg'=>"Your contact inquiry has been successfully sent."]);
		}
	}

	public function subscribe(){
		$check = Subscriber::where('email',Input::get('email'))->first();
		if($check){
			$check->is_subscribe = 'yes';
			if($check->save()){
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"You're already subscribe to our news and updates.");

				$notification_data = new SendEmail(['input' => Input::all(),'type' => "subscribe"]);
				Event::fire('send_email', $notification_data);

				return redirect()->intended('/#subscribe');
			}
		}else{
			$new_subscribers = new Subscriber;
			$new_subscribers->fill(Input::all());

			$new_subscribers->is_subscribe = 'yes';

			if($new_subscribers->save()){
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"You're subscription is successfull.");

				$notification_data = new SendEmail(['input' => Input::all(),'type' => "subscribe"]);
				Event::fire('send_email', $notification_data);

				return redirect()->intended('https://www.bistro.com.ph/newsletter/register.php');
			}
		}
	}

	public function unsubscribe($email = NULL){
		$unsubscribe = Subscriber::where('email',$email)->first();

		$unsubscribe->is_subscribe = 'no';

		if($unsubscribe->save()){
			Session::flash('notification-status','success');
			Session::flash('notification-msg',"You're unsubscription is successfull.");

			return redirect()->route('frontend.index');
		}
	}
}