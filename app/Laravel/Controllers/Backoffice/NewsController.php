<?php namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\News;
use App\Laravel\Models\MenuCategory;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\NewsRequest;
use App\Laravel\Requests\Backoffice\EditNewsRequest;

/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image, ImageUploader;

class NewsController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		parent::__construct();
		$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "News";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "news";
	}

	public function index () {
		$this->data['data'] = News::orderBy('created_at',"DESC")->get();
		return view('backoffice.'.$this->data['route_file'].'.index',$this->data);
	}

	public function store (NewsRequest $request) {
		try {
			$new_data = new News;
			$new_data->fill($request->all());

			if($request->hasFile('file')){
				$upload = ImageUploader::upload($request->file,'storage/news');
				$new_data->path = $upload["path"];
				$new_data->directory = $upload["directory"];
				$new_data->filename = $upload["filename"];
			}

			if($new_data->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"New data has been added.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}


	public function update (EditNewsRequest $request, $id = NULL) {
		try {
			$data = News::find($id);

			if (!$data) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			$data->fill($request->all());

			if($request->hasFile('file')){
				$upload = ImageUploader::upload($request->file,'storage/data');
				if($upload){	
					if (File::exists("{$data->directory}/{$data->filename}")){
						File::delete("{$data->directory}/{$data->filename}");
					}
					if (File::exists("{$data->directory}/resized/{$data->filename}")){
						File::delete("{$data->directory}/resized/{$data->filename}");
					}
					if (File::exists("{$data->directory}/thumbnails/{$data->filename}")){
						File::delete("{$data->directory}/thumbnails/{$data->filename}");
					}
				}
				
				$data->path = $upload["path"];
				$data->directory = $upload["directory"];
				$data->filename = $upload["filename"];
			}

			if($data->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A data has been updated.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$data = News::find($id);

			if (!$data) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			if (File::exists("{$data->directory}/{$data->filename}")){
				File::delete("{$data->directory}/{$data->filename}");
			}
			if (File::exists("{$data->directory}/resized/{$data->filename}")){
				File::delete("{$data->directory}/resized/{$data->filename}");
			}
			if (File::exists("{$data->directory}/thumbnails/{$data->filename}")){
				File::delete("{$data->directory}/thumbnails/{$data->filename}");
			}

			if($data->save() AND $data->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A data has been deleted.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

}