<?php namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Slider;
use App\Laravel\Models\Brand;


/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\BrandRequest;

/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image, ImageUploader;

class BrandsController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		parent::__construct();
		$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "Brands";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "brands";
		
	}

	public function index () {
		$this->data['brands'] = Brand::orderBy('created_at',"DESC")->get();
		return view('backoffice.'.$this->data['route_file'].'.index',$this->data);
	}

	public function create(){
		return view('backoffice.'.$this->data['route_file'].'.create',$this->data);
	}

	public function store (BrandRequest $request) {
		try {
			$new_data = new Brand;
			$new_data->fill($request->all());

			if($request->hasFile('image')){
				$image = $request->file('image');
				$upload = ImageUploader::upload($image,'storage/logo');
				$new_data->path = $upload["path"];
				$new_data->directory = $upload["directory"];
				$new_data->filename = $upload["filename"];
			}

			if($request->hasFile('thumbnail')){
				$image = $request->file('thumbnail');
				$upload = ImageUploader::upload($image,'storage/brands');
				$new_data->path_logo = $upload["path"];
				$new_data->directory_logo = $upload["directory"];
				$new_data->filename_logo = $upload["filename"];
			}

			if($new_data->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"New data has been added.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$brand = Brand::find($id);

		if (!$brand) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
		}

		$this->data['brand'] = $brand;
		return view('backoffice.'.$this->data['route_file'].'.edit',$this->data);
	}

	public function update (BrandRequest $request, $id = NULL) {
		try {
			$data = Brand::find($id);

			if (!$data) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			$data->fill($request->all());

			if($request->hasFile('image')){
				$image = $request->file('image');
				$upload = ImageUploader::upload($image,'storage/logo');
				if($upload){	
					if (File::exists("{$data->directory_logo}/{$data->filename_logo}")){
						File::delete("{$data->directory_logo}/{$data->filename_logo}");
					}
					if (File::exists("{$data->directory_logo}/resized/{$data->filename_logo}")){
						File::delete("{$data->directory_logo}/resized/{$data->filename_logo}");
					}
					if (File::exists("{$data->directory_logo}/thumbnails/{$data->filename_logo}")){
						File::delete("{$data->directory_logo}/thumbnails/{$data->filename_logo}");
					}
				}
				
				$data->path_logo = $upload["path"];
				$data->directory_logo = $upload["directory"];
				$data->filename_logo = $upload["filename"];
			}

			if($request->hasFile('thumbnail')){
				$image = $request->file('thumbnail');
				$upload = ImageUploader::upload($image,'storage/brands');
				if($upload){	
					if (File::exists("{$data->directory}/{$data->filename}")){
						File::delete("{$data->directory}/{$data->filename}");
					}
					if (File::exists("{$data->directory}/resized/{$data->filename}")){
						File::delete("{$data->directory}/resized/{$data->filename}");
					}
					if (File::exists("{$data->directory}/thumbnails/{$data->filename}")){
						File::delete("{$data->directory}/thumbnails/{$data->filename}");
					}
				}
				
				$data->path = $upload["path"];
				$data->directory = $upload["directory"];
				$data->filename = $upload["filename"];
			}

			

			if($data->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A data has been updated.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$data = Brand::find($id);

			if (!$data) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			if (File::exists("{$data->directory}/{$data->filename}")){
				File::delete("{$data->directory}/{$data->filename}");
			}
			if (File::exists("{$data->directory}/resized/{$data->filename}")){
				File::delete("{$data->directory}/resized/{$data->filename}");
			}
			if (File::exists("{$data->directory}/thumbnails/{$data->filename}")){
				File::delete("{$data->directory}/thumbnails/{$data->filename}");
			}

			if($data->save() AND $data->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A data has been deleted.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

}