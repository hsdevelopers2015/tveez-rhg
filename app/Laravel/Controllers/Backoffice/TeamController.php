<?php namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Slider;
use App\Laravel\Models\Brand;
use App\Laravel\Models\Team;


/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\BrandRequest;
use App\Laravel\Requests\Backoffice\TeamRequest;

/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image, ImageUploader;

class TeamController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		parent::__construct();
		$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "Team";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "team";
		
	}

	public function index () {
		$this->data['team'] = Team::orderBy('created_at',"DESC")->get();
		return view('backoffice.'.$this->data['route_file'].'.index',$this->data);
	}

	public function create(){
		return view('backoffice.'.$this->data['route_file'].'.create',$this->data);
	}

	public function store (TeamRequest $request) {
		
		try {
			$new_data = new Team;
			$new_data->fill($request->all());

			if($request->hasFile('file')){
				$upload = ImageUploader::upload($request->file,'storage/Team');
				$new_data->path = $upload["path"];
				$new_data->directory = $upload["directory"];
				$new_data->filename = $upload["filename"];
			}

			if($new_data->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"New data has been added.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$team = Team::find($id);

		if (!$team) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
		}

		$this->data['team'] = $team;
		return view('backoffice.'.$this->data['route_file'].'.edit',$this->data);
	}

	public function update (TeamRequest $request, $id = NULL) {
		try {
			$data = Team::find($id);

			if (!$data) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			$data->fill($request->all());

			if($request->hasFile('file')){
				$upload = ImageUploader::upload($request->file,'storage/sliders');
				if($upload){	
					if (File::exists("{$data->directory}/{$data->filename}")){
						File::delete("{$data->directory}/{$data->filename}");
					}
					if (File::exists("{$data->directory}/resized/{$data->filename}")){
						File::delete("{$data->directory}/resized/{$data->filename}");
					}
					if (File::exists("{$data->directory}/thumbnails/{$data->filename}")){
						File::delete("{$data->directory}/thumbnails/{$data->filename}");
					}
				}
				
				$data->path = $upload["path"];
				$data->directory = $upload["directory"];
				$data->filename = $upload["filename"];
			}

			if($data->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A data has been updated.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$data = Slider::find($id);

			if (!$data) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			if (File::exists("{$data->directory}/{$data->filename}")){
				File::delete("{$data->directory}/{$data->filename}");
			}
			if (File::exists("{$data->directory}/resized/{$data->filename}")){
				File::delete("{$data->directory}/resized/{$data->filename}");
			}
			if (File::exists("{$data->directory}/thumbnails/{$data->filename}")){
				File::delete("{$data->directory}/thumbnails/{$data->filename}");
			}

			if($data->save() AND $data->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A data has been deleted.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

}