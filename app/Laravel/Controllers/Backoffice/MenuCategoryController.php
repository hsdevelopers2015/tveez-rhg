<?php namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\MenuCategory;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\MenuCategoryRequest;
use App\Laravel\Requests\Backoffice\EditMenuCategoryRequest;

/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image, ImageUploader;

class MenuCategoryController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		parent::__construct();
		$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "Menu Categories";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "menu_category";
		$this->data['has_parent'] = ['no'=>'No','yes'=>"Yes"];
		$this->data['parent'] = ['' => "Choose parent"] + MenuCategory::pluck('category_name','id')->toArray();
	}

	public function index () {
		$this->data['data'] = MenuCategory::orderBy('created_at',"DESC")->get();
		return view('backoffice.'.$this->data['route_file'].'.index',$this->data);
	}

	public function store (MenuCategoryRequest $request) {
		try {
			$new_data = new MenuCategory;
			$new_data->fill($request->all());

			if($request->hasFile('file')){
				$upload = ImageUploader::upload($request->file,'storage/menu_category');
				$new_data->path = $upload["path"];
				$new_data->directory = $upload["directory"];
				$new_data->filename = $upload["filename"];
			}

			if($new_data->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"New data has been added.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function update (EditMenuCategoryRequest $request, $id = NULL) {
		try {
			$data = MenuCategory::find($id);

			if (!$data) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			$data->fill($request->all());

			if($request->hasFile('file')){
				$upload = ImageUploader::upload($request->file,'storage/data');
				if($upload){	
					if (File::exists("{$data->directory}/{$data->filename}")){
						File::delete("{$data->directory}/{$data->filename}");
					}
					if (File::exists("{$data->directory}/resized/{$data->filename}")){
						File::delete("{$data->directory}/resized/{$data->filename}");
					}
					if (File::exists("{$data->directory}/thumbnails/{$data->filename}")){
						File::delete("{$data->directory}/thumbnails/{$data->filename}");
					}
				}
				
				$data->path = $upload["path"];
				$data->directory = $upload["directory"];
				$data->filename = $upload["filename"];
			}

			if($data->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A data has been updated.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$data = MenuCategory::find($id);

			if (!$data) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			if (File::exists("{$data->directory}/{$data->filename}")){
				File::delete("{$data->directory}/{$data->filename}");
			}
			if (File::exists("{$data->directory}/resized/{$data->filename}")){
				File::delete("{$data->directory}/resized/{$data->filename}");
			}
			if (File::exists("{$data->directory}/thumbnails/{$data->filename}")){
				File::delete("{$data->directory}/thumbnails/{$data->filename}");
			}

			if($data->save() AND $data->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A data has been deleted.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

}