<?php namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\SocialLink;
use App\Laravel\Models\MenuCategory;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\SocialLinkRequest;
use App\Laravel\Requests\Backoffice\EditSocialLinkRequest;

/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image, ImageUploader;

class SocialLinkController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		parent::__construct();
		$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "SocialLink";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "social_links";
		$this->data['types'] = [ '' => "Choose type", 'facebook' => "Facebook", 'twitter' => "Twitter",'youtube' => "Youtube",'pinterest' => "Pinterest",'instagram' => "Instagram",'google' => "Google",'google-plus' => "Google Plus", 'github'=>"Github",'wordpress'=>"Wordpress",'vimeo'=>"Vimeo",'skype'=>"Skype",'spotify'=>"Spotify"];
	}

	public function index () {
		$this->data['data'] = SocialLink::orderBy('created_at',"DESC")->get();
		return view('backoffice.'.$this->data['route_file'].'.index',$this->data);
	}

	public function create () {
		return view('backoffice.'.$this->data['route_file'].'.create',$this->data);
	}

	public function store (SocialLinkRequest $request) {
		try {
			$new_data = new SocialLink;
			$new_data->fill($request->all());

			if($new_data->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"New data has been added.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$social_link = SocialLink::find($id);

		if (!$social_link) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
		}

		$this->data['social_link'] = $social_link;
		return view('backoffice.'.$this->data['route_file'].'.edit',$this->data);
	}


	public function update (SocialLinkRequest $request, $id = NULL) {
		try {
			$data = SocialLink::find($id);

			if (!$data) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			$data->fill($request->all());

			if($data->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A data has been updated.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$data = SocialLink::find($id);

			if (!$data) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			if (File::exists("{$data->directory}/{$data->filename}")){
				File::delete("{$data->directory}/{$data->filename}");
			}
			if (File::exists("{$data->directory}/resized/{$data->filename}")){
				File::delete("{$data->directory}/resized/{$data->filename}");
			}
			if (File::exists("{$data->directory}/thumbnails/{$data->filename}")){
				File::delete("{$data->directory}/thumbnails/{$data->filename}");
			}

			if($data->save() AND $data->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A data has been deleted.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

}