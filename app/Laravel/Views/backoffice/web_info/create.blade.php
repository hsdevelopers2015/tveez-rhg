@extends('backoffice._layouts.main')

@section('content')
	<div class="be-content">
        <div class="page-head">
            <h2 class="page-head-title">{{$page_title}} Form</h2>
            <ol class="breadcrumb page-head-nav">
                <li>
                    <a href="{{route('backoffice.dashboard')}}">Home</a>
                </li>
                <li>
                    <a href="{{route('backoffice.'.$route_file.'.index')}}">All Records</a>
                </li>
                <li class="active">Create</li>
            </ol>
        </div>
        <div class="main-content container-fluid">
            <!--Basic forms-->
            <div class="row">
                <div class="col-sm-8">
                    @include('backoffice._components.notification')
                    <div class="panel panel-default panel-border-color panel-border-color-primary">
                        <div class="panel-heading panel-heading-divider">
                            {{$page_title}} Form<span class="panel-subtitle">{{$page_description}}</span>
                        </div>
                        <div class="panel-body">
                            <form method="POST" action="" enctype="multipart/form-data">
                            	<input type="hidden" name="_token" value="{{csrf_token()}}">
                                 <div class="form-group">
                                    <label class="control-label">Current Thumbnail</label>
                                    <br>
                                    <img src="{{asset($web_info->directory.'/'.$web_info->filename)}}" alt="" class="img-thumbnail img-responsive">
                                </div>
                                <div class="form-group {{$errors->first('file') ? 'has-error' : NULL}}">
                                    <label class="control-label">Choose Logo </label>
                                    <br>
                                    <input type="file" name="file" id="file-1" data-multiple-caption="{count} files selected" multiple="" class="inputfile">
                                    <label for="file-1" class="btn-default"> <i class="mdi mdi-upload"></i><span>Choose a file</span></label>
                                    <span class="help-block">Image dimension <code>960 x 497 px</code></span>
                                    @if($errors->first('file'))
                                    <span class="help-block">{!!$errors->first('file')!!}</span>
                                    @endif
                                </div>
                                <div class="form-group {{$errors->first('description') ? 'has-error' : NULL}}">
                                    <label class="control-label">Description</label> <input class="form-control input-sm" value="{{old('description',$web_info->description)}}" placeholder="description" type="text" name="description">
                                    @if($errors->first('description'))
                                    <span class="help-block">{!!$errors->first('description')!!}</span>
                                    @endif
                                </div>
                                <div class="form-group {{$errors->first('address') ? 'has-error' : NULL}}">
                                    <label class="control-label">Address</label> <input class="form-control input-sm" value="{{old('address',$web_info->address)}}" placeholder="address" type="text" name="address">
                                    @if($errors->first('address'))
                                    <span class="help-block">{!!$errors->first('address')!!}</span>
                                    @endif
                                </div>
                                <div class="form-group {{$errors->first('phone') ? 'has-error' : NULL}}">
                                    <label class="control-label">Phone</label> <input class="form-control input-sm" value="{{old('phone',$web_info->phone)}}" placeholder="Phone" type="text" name="phone">
                                    @if($errors->first('phone'))
                                    <span class="help-block">{!!$errors->first('phone')!!}</span>
                                    @endif
                                </div>
                                <div class="form-group {{$errors->first('operations') ? 'has-error' : NULL}}">
                                    <label class="control-label">Operations</label> <input class="form-control input-sm" value="{{old('operations',$web_info->operations)}}" placeholder="operations" type="text" name="operations">
                                    @if($errors->first('operations'))
                                    <span class="help-block">{!!$errors->first('operations')!!}</span>
                                    @endif
                                </div>
                                <div class="form-group {{$errors->first('fax') ? 'has-error' : NULL}}">
                                    <label class="control-label">Fax</label> <input class="form-control input-sm" value="{{old('fax',$web_info->fax)}}" placeholder="Fax" type="text" name="fax">
                                    @if($errors->first('fax'))
                                    <span class="help-block">{!!$errors->first('fax')!!}</span>
                                    @endif
                                </div>
                                <div class="form-group {{$errors->first('email') ? 'has-error' : NULL}}">
                                    <label class="control-label">Email</label> <input class="form-control input-sm" value="{{old('email',$web_info->email)}}" placeholder="Email" type="text" name="email">
                                    @if($errors->first('email'))
                                    <span class="help-block">{!!$errors->first('email')!!}</span>
                                    @endif
                                </div>
                                
                                <div class="form-group {{ $errors->has('geo_lat') ? "has-danger" : NULL }} row">
                                  <label class="col-md-2 label-control" for="geo_lat">Geo Latitude</label>
                                  <div class="col-md-9">
                                    <input type="text" id="geo_lat" class="form-control input-sm" placeholder="Geo Latitude" name="geo_lat" id="geo_lat" value="{{ old('geo_lat',$web_info->geo_lat) }}">
                                    @if($errors->has('geo_lat')) <p class="text-xs-left"><small class="danger text-muted">{{ $errors->first('geo_lat') }}</small></p> @endif
                                  </div>
                                </div>
                                <div class="form-group {{ $errors->has('geo_long') ? "has-danger" : NULL }} row">
                                  <label class="col-md-2 label-control" for="geo_long">Geo Longitude</label>
                                  <div class="col-md-9">
                                    <input type="text" id="geo_long" class="form-control input-sm" placeholder="Geo Longitude" name="geo_long" id="geo_long" value="{{ old('geo_long',$web_info->geo_long) }}">
                                    @if($errors->has('geo_long')) <p class="text-xs-left"><small class="danger text-muted">{{ $errors->first('geo_long') }}</small></p> @endif
                                  </div>
                                </div>

                                <div class="form-group row">
                                  <label class="col-md-2 label-control" for="device_type">Geo-location</label>
                                  <div class="col-md-9">
                                    <div class="input-group">
                                      <span class="input-group-addon"><i class="icon-search"></i></span>
                                      <input type="text" id="map-address" class="form-control disableSubmitOnEnter input-sm" placeholder="Search Location" name="map_address">
                                    </div>
                                    <input type="hidden" name="street_address" id="street_address">
                                    <input type="hidden" name="city" id="city">
                                    <input type="hidden" name="state" id="state">
                                    <input type="hidden" name="country" id="country">
                                    <div id="map" style="width: 100%; height: 300px;"></div>
                                  </div>
                                </div>



                                <div class="row xs-pt-15">
                                	<div class="col-xs-12">
                                		<p class="text-right">
                                			<button class="btn btn-space btn-primary" type="submit">Submit</button> 
                                			<a href="{{route('backoffice.'.$route_file.'.index')}}" class="btn btn-space btn-default">Cancel</a>
                                		</p>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('backoffice/assets/lib/material-design-icons/css/material-design-iconic-font.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/assets/lib/daterangepicker/css/daterangepicker.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/assets/lib/select2/css/select2.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/assets/lib/bootstrap-slider/css/bootstrap-slider.css')}}"/>
<link rel="stylesheet" href="{{asset('backoffice/assets/css/style.css')}}" type="text/css"/>
@stop

@section('page-scripts')
<script src="{{asset('backoffice/assets/lib/jquery/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/assets/js/main.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/assets/lib/bootstrap/dist/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/assets/lib/jquery-ui/jquery-ui.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/assets/lib/jquery.nestable/jquery.nestable.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/assets/lib/daterangepicker/js/daterangepicker.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/assets/lib/select2/js/select2.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/assets/lib/select2/js/select2.full.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/assets/lib/bootstrap-slider/js/bootstrap-slider.js')}}" type="text/javascript"></script>
<script src="http://maps.google.com/maps/api/js?sensor=true&v=3&libraries=places&key={{env('GOOGLE_MAP_KEY')}}"></script>
<script src="{{asset('backoffice/assets/js/locationpicker/locationpicker.jquery.js')}}"></script>
<script type="text/javascript">
  $(document).ready(function(){
        //initialize the javascript
        App.init();
        App.formElements();
    });
</script>
<link href="{{ asset('backoffice/summernote/summernote.css')}}" rel="stylesheet"> 
<script src="{{ asset('backoffice/summernote/summernote.min.js')}}"></script> 
{{-- <script src="{{asset('frontend/summernote/summernote-cleaner.js')}}"></script> --}}
<script type="text/javascript">     
    $(function(){           
        function updateControls(addressComponents) {
          $('#street_address').val(addressComponents.addressLine1);
          $('#city').val(addressComponents.city);
          $('#state').val(addressComponents.stateOrProvince);
          $('#country').val(addressComponents.country);
        }

        

        $('#map').locationpicker({
            location: {
                latitude: {{ old('geo_lat', $web_info->geo_lat ? : 14.5995) }},
                longitude: {{ old('geo_long', $web_info->geo_long ?: 120.9842) }}
            },
            radius: 50,
            inputBinding : {
              locationNameInput: $('#map-address'),
              latitudeInput: $('#geo_lat'),
              longitudeInput: $('#geo_long'),
            },
            enableAutocomplete: true,
            autocompleteOptions: {
                // types: ['(cities)'],
                componentRestrictions: {country: 'ph'}
            },
            onchanged: function (currentLocation, radius, isMarkerDropped) {
                var addressComponents = $(this).locationpicker('map').location.addressComponents;
                updateControls(addressComponents);
            },
            oninitialized: function(component) {
                var addressComponents = $(component).locationpicker('map').location.addressComponents;
                updateControls(addressComponents);
            }
        });
        $(".summernote").summernote({ 
            height : 300,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'italic', 'underline', 'clear', 'superscript', 'subscript']],
                ['fontname', ['fontname']],
                ['color', ['color']],
                ['fontsize', ['fontsize']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table',['table']],
                ['insert',['link','picture','video']],
                ['view',[/*'fullscreen',*/'codeview','help','undo','redo']]
            ],
            fontNames:["Arial","Arial Black","Comic Sans MS","Courier New","Helvetica Neue","Helvetica","Impact","Lucida Grande","Tahoma","Times New Roman","Verdana","Roboto"],

            onImageUpload: function(files, editor, welEditable) {
                sendFile(files[0], editor, welEditable);
            },
            cleaner:{
                 notTime: 2400, // Time to display Notifications.
                 action: 'both', // both|button|paste 'button' only cleans via toolbar button, 'paste' only clean when pasting content, both does both options.
                 newline: '<br>', // Summernote's default is to use '<p><br></p>'
                 notStyle: 'position:absolute;top:0;left:0;right:0', // Position of Notification
                 icon: '<i class="note-icon">Reset</i>',
                 keepHtml: false, // Remove all Html formats
                 keepClasses: false, // Remove Classes
                 badTags: ['style', 'script', 'applet', 'embed', 'noframes', 'noscript', 'html'], // Remove full tags with contents
                 badAttributes: ['style', 'start'] // Remove attributes from remaining tags
            },
            dialogsFade: true,
            dialogsInBody: true,
            placeholder: 'Description'
        }); 

        function sendFile(file, editor, welEditable) {
            data = new FormData();
            data.append("file", file);
            data.append("api_token","{{env('APP_KEY')}}");
            $.ajax({
                data: data,
                type: "POST",
                url: "{{route('api.summernote',['json'])}}",
                cache: false,
                contentType: false,
                processData: false,
                success: function(data) {
                 if(data.status == true){
                  $('.summernote').summernote('insertImage', data.image);
                 }
                }
            });
        }       
    }); 
</script>
@stop
