@extends('backoffice._layouts.main')

@section('content')
<div class="be-content">
        <div class="page-head">
            <h2 class="page-head-title">List of {{$page_title}}</h2>
            <ol class="breadcrumb page-head-nav">
                <li>
                    <a href="{{route('backoffice.dashboard')}}">Home</a>
                </li>
                <li class="active">All Records</li>
            </ol>
        </div>
        <div class="main-content container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    @include('backoffice._components.notification')
                    <div class="panel panel-default panel-table">
                        <div class="panel-heading">
                            {{$page_title}}
                            <div class="tools dropdown">
                                <a href="#" data-toggle="modal" data-target=".create"><span class="icon mdi mdi-plus"></span></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <table class="table table-striped table-hover table-fw-widget" id="table1">
                                <thead>
                                    <tr>
                                        <th style="width: 5%;">#</th>
                                        <th>Thumbnail</th>
                                        <th>Category Name</th>
                                        <th>Last Update</th>
                                        <th class="actions"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                	@foreach($data as $index => $info)
                                    <tr class="odd gradeX">
                                        <td>{{$index+1}}</td>
                                        <td>
                                            <img src="{{asset($info->directory.'/'.$info->filename)}}" alt="" class="img-thumbnail" width="80">
                                        </td>
                                        <td>{{$info->category_name}}</td>
                                        <td>{{Helper::date_format($info->updated_at,'F d, Y')}}</td>
                                        <td class="actions">
                                        	<div class="btn-group btn-hspace">
                                        		<button type="button" data-toggle="dropdown" class="btn btn-default dropdown-toggle">Action <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                                        		<ul role="menu" class="dropdown-menu pull-right">
                                        			<li>
                                        				<a href="#" data-toggle="modal" data-target=".edit" 
                                                        data-image="{{asset($info->directory.'/'.$info->filename)}}"
                                                        data-category_name="{{$info->category_name}}" class="action-view"
                                                        data-url="{{route('backoffice.'.$route_file.'.update',$info->id)}}">Edit</a>
                                        			</li>
                                        			<li>
                                        				<a href="#" data-toggle="modal" data-url="{{route('backoffice.'.$route_file.'.destroy',[$info->id])}}" data-target="#delete" class="action-delete">Delete</a>
                                        			</li>
                                        		</ul>
                                        	</div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('page-modals')
<div id="delete" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <div class="text-danger"><span class="modal-main-icon mdi mdi-close-circle-o"></span></div>
                    <h3>Danger!</h3>
                    <p>This action can not be undone.<br>You are about to delete a record, this action can no longer be undone,<br> are you sure you want to proceed?</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
                <a type="button" class="btn btn-danger" id="btn-confirm-delete">Proceed</a>
            </div>
        </div>
    </div>
</div>

<div tabindex="-1" role="dialog" class="modal fade edit">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
                <h4>Edit Menu Category</h4>
            </div>
            {{ Form::open(['class' => "form-edit" ,'data-parsley-validate' => " " ,'novalidate' => " ", 'enctype' => "multipart/form-data"] ) }}
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <small>Category Name</small>
                            <input type="text" name="category_name" class="input-sm form-control has-error menu-category-name" value="" parsley-trigger="change" required="">
                        </div>
                        <br>
                        <div class="form-group">
                            <small>Has Parent?</small>
                            {{Form::select('has_parent',$has_parent,old('has_parent'),['class'=>"form-control input-sm has-parent-field"])}}
                        </div>
                        <div class="form-group parent-div">
                        <br>
                            <small>Parent</small>
                            {{Form::select('parent_id',$parent,old('parent_id'),['class'=>"form-control input-sm parent-field"])}}
                        </div>
                        <br>
                        <div class="form-group">
                            <small>Image Header</small>
                            <input type="file" name="file" class="input-sm form-control has-error" value="" parsley-trigger="change" parsley-type="file">
                        </div>
                    </div>
                </div>
                <small>Current Image :</small>
                <div class="row">
                    <div class="col-md-6">
                        <img class="menu-image img img-thumbnail" />
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            {{Form::close()}}
        </div>
    </div>
</div>

<div tabindex="-1" role="dialog" class="modal fade create">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
                <h4>
                    Create Menu Category
                </h4>
            </div>
            {{ Form::open(['route' => 'backoffice.menu_category.store','data-parsley-validate' => " " ,'novalidate' => " ", 'enctype' => "multipart/form-data"] ) }}
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <small>Category Name</small>
                            <input type="text" name="category_name" class="input-sm form-control has-error" value="" parsley-trigger="change" required="">
                        </div>
                        <br>
                        <div class="form-group">
                            <small>Has Parent?</small>
                            {{Form::select('has_parent',$has_parent,old('has_parent'),['class'=>"form-control input-sm has-parent-field"])}}
                        </div>
                        <div class="form-group parent-div">
                        <br>
                            <small>Parent</small>
                            {{Form::select('parent_id',$parent,old('parent_id'),['class'=>"form-control input-sm parent-field"])}}
                        </div>
                        <br>
                        <div class="form-group">
                            <small>Image Header</small>
                            <input type="file" name="file" class="input-sm form-control image-field has-error" value="" parsley-trigger="change" parsley-type="file" required="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            {{Form::close()}}
        </div>
    </div>
</div>
@stop

@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/assets/lib/material-design-icons/css/material-design-iconic-font.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/assets/lib/datatables/css/dataTables.bootstrap.min.css')}}"/>
<link rel="stylesheet" href="{{asset('backoffice/assets/css/style.css')}}" type="text/css"/>
<style type="text/css">
    .m-b-20{
        margin-bottom: 20px;
    }
    .parsley-errors-list.filled {
        padding: 5px 10px!important;
    }
    .input-sm{
        height: 30px!important;
        font-size: 12px!important;
    }
</style>
@stop

@section('page-scripts')
<script src="{{asset('backoffice/assets/lib/jquery/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/assets/js/main.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/assets/lib/bootstrap/dist/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/assets/lib/datatables/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/assets/lib/datatables/js/dataTables.bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/assets/lib/datatables/plugins/buttons/js/dataTables.buttons.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/assets/lib/datatables/plugins/buttons/js/buttons.html5.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/assets/lib/datatables/plugins/buttons/js/buttons.flash.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/assets/lib/datatables/plugins/buttons/js/buttons.print.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/assets/lib/datatables/plugins/buttons/js/buttons.colVis.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/assets/lib/datatables/plugins/buttons/js/buttons.bootstrap.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/assets/lib/parsley/parsley.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function(){
        //initialize the javascript
        App.init();
        App.dataTables();
    });
  
    $(".action-delete").on("click",function(){
        var btn = $(this);
        $("#btn-confirm-delete").attr({"href" : btn.data('url')});
    });
  
    $(".action-view").on("click",function(){
        var btn = $(this);
        $(".form-edit").attr({"action" : btn.data('url')});
        $(".menu-category-name").val(btn.data('category_name'));
        $(".menu-image").attr({"src" : btn.data('image')});
    });

    $(".has-parent-field").on("change", function(){
        var value = $(this);

        if(value.val() == "no"){
            $(".parent-div").hide(250);
            $(".image-field").attr('required',"true");
            $(".parent-field").removeAttr('required');
        }else{
            $(".parent-div").show(250);
            $(".image-field").removeAttr('required');
            $(".parent-field").attr('required',"true");
        }
    });

    $(".parent-div").hide();
</script>
@stop
