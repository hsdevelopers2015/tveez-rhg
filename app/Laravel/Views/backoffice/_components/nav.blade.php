<div class="be-left-sidebar">
    <div class="left-sidebar-wrapper">
        <a class="left-sidebar-toggle" href="#">Dashboard</a>
        <div class="left-sidebar-spacer">
            <div class="left-sidebar-scroll">
                <div class="left-sidebar-content">
                    <ul class="sidebar-elements">
                        <li class="divider">Menu</li>

                        <li class="{{ active_class(if_route(['backoffice.dashboard']), 'active') }}">
                            <a href="{{route('backoffice.dashboard')}}"><i class="icon mdi mdi-home"></i><span>Dashboard</span></a>
                        </li>

                        <li class="{{ active_class(if_route(['backoffice.contact_inquiry.index']), 'active') }}">
                            <a href="{{route('backoffice.contact_inquiry.index')}}"><i class="icon mdi mdi-cast"></i><span>Contact Inquiries</span></a>
                        </li>
                        <li class="{{ active_class(if_route(['backoffice.web_info.index']), 'active') }}">
                            <a href="{{route('backoffice.web_info.index')}}"><i class="icon mdi mdi-cast"></i><span>Web Information</span></a>
                        </li>

                        <li class="parent {{ active_class(if_route(['backoffice.social_links.index','backoffice.social_links.create','backoffice.social_links.edit','backoffice.social_links.destroy']), 'active open') }}">
                            <a href="#"><i class="icon mdi mdi-cast"></i><span>social_links</span></a>
                            <ul class="sub-menu">
                                <li class="{{ active_class(if_route(['backoffice.social_links.index',]), 'active open') }}">
                                    <a href="{{route('backoffice.social_links.index')}}">All Records</a>
                                </li>
                                <li class="{{ active_class(if_route(['backoffice.social_links.create',]), 'active open') }}">
                                    <a href="{{route('backoffice.social_links.create')}}">Create New</a>
                                </li>
                            </ul>
                        </li>

                        
                        
                        <li class="divider">Web Content</li>

                         <li class="parent {{ active_class(if_route(['backoffice.sliders.index','backoffice.sliders.create','backoffice.sliders.edit','backoffice.sliders.destroy']), 'active open') }}">
                            <a href="#"><i class="icon mdi mdi-cast"></i><span>Sub Content</span></a>
                            <ul class="sub-menu">
                                <li class="{{ active_class(if_route(['backoffice.sub_content.index',]), 'active open') }}">
                                    <a href="{{route('backoffice.sub_content.index')}}">All Records</a>
                                </li>
                                <li class="{{ active_class(if_route(['backoffice.sub_content.create',]), 'active open') }}">
                                    <a href="{{route('backoffice.sub_content.create')}}">Create New</a>
                                </li>
                            </ul>
                        </li>

                         <li class="parent {{ active_class(if_route(['backoffice.sliders.index','backoffice.sliders.create','backoffice.sliders.edit','backoffice.sliders.destroy']), 'active open') }}">
                            <a href="#"><i class="icon mdi mdi-cast"></i><span>Sliders</span></a>
                            <ul class="sub-menu">
                                <li class="{{ active_class(if_route(['backoffice.sliders.index',]), 'active open') }}">
                                    <a href="{{route('backoffice.sliders.index')}}">All Records</a>
                                </li>
                                <li class="{{ active_class(if_route(['backoffice.sliders.create',]), 'active open') }}">
                                    <a href="{{route('backoffice.sliders.create')}}">Create New</a>
                                </li>
                            </ul>
                        </li>
                         <li class="parent {{ active_class(if_route(['backoffice.brands.index','backoffice.brands.create','backoffice.brands.edit','backoffice.brands.destroy']), 'active open') }}">
                            <a href="#"><i class="icon mdi mdi-cast"></i><span>Brands</span></a>
                            <ul class="sub-menu">
                                <li class="{{ active_class(if_route(['backoffice.brands.index',]), 'active open') }}">
                                    <a href="{{route('backoffice.brands.index')}}">All Records</a>
                                </li>
                                <li class="{{ active_class(if_route(['backoffice.brands.create',]), 'active open') }}">
                                    <a href="{{route('backoffice.brands.create')}}">Create New</a>
                                </li>
                            </ul>
                        </li>
                        <li class="parent {{ active_class(if_route(['backoffice.partners.index','backoffice.partners.create','backoffice.partners.edit','backoffice.partners.destroy']), 'active open') }}">
                            <a href="#"><i class="icon mdi mdi-cast"></i><span>Partners</span></a>
                            <ul class="sub-menu">
                                <li class="{{ active_class(if_route(['backoffice.partners.index',]), 'active open') }}">
                                    <a href="{{route('backoffice.partners.index')}}">All Records</a>
                                </li>
                                <li class="{{ active_class(if_route(['backoffice.partners.create',]), 'active open') }}">
                                    <a href="{{route('backoffice.partners.create')}}">Create New</a>
                                </li>
                            </ul>
                        </li>
                        <li class="parent {{ active_class(if_route(['backoffice.team.index','backoffice.team.create','backoffice.team.edit','backoffice.team.destroy']), 'active open') }}">
                            <a href="#"><i class="icon mdi mdi-cast"></i><span>Team</span></a>
                            <ul class="sub-menu">
                                <li class="{{ active_class(if_route(['backoffice.team.index',]), 'active open') }}">
                                    <a href="{{route('backoffice.team.index')}}">All Records</a>
                                </li>
                                <li class="{{ active_class(if_route(['backoffice.team.create',]), 'active open') }}">
                                    <a href="{{route('backoffice.team.create')}}">Create New</a>
                                </li>
                            </ul>
                        </li>

                        <li class="parent {{ active_class(if_route(['backoffice.career.index','backoffice.career.create','backoffice.career.edit','backoffice.career.destroy']), 'active open') }}">
                            <a href="#"><i class="icon mdi mdi-cast"></i><span>Career</span></a>
                            <ul class="sub-menu">
                                <li class="{{ active_class(if_route(['backoffice.career.index',]), 'active open') }}">
                                    <a href="{{route('backoffice.career.index')}}">All Records</a>
                                </li>
                                <li class="{{ active_class(if_route(['backoffice.career.create',]), 'active open') }}">
                                    <a href="{{route('backoffice.career.create')}}">Create New</a>
                                </li>
                            </ul>
                        </li>

                         <li class="parent {{ active_class(if_route(['backoffice.page_content.index','backoffice.page_content.create','backoffice.page_content.edit','backoffice.page_content.destroy']), 'active open') }}">
                            <a href="#"><i class="icon mdi mdi-cast"></i><span>Pages    </span></a>
                            <ul class="sub-menu">
                                <li class="{{ active_class(if_route(['backoffice.page_content.index',]), 'active open') }}">
                                    <a href="{{route('backoffice.page_content.index')}}">All Records</a>
                                </li>
                                <li class="{{ active_class(if_route(['backoffice.page_content.create',]), 'active open') }}">
                                    <a href="{{route('backoffice.page_content.create')}}">Create New</a>
                                </li>
                            </ul>
                        </li>

                        <li class="divider">User Management</li>
                       
                    </ul>
                </div>
            </div>
        </div>
        {{-- <div class="progress-widget">
            <div class="progress-data">
                <span class="progress-value">60%</span><span class="name">Current Project</span>
            </div>
            <div class="progress">
                <div class="progress-bar progress-bar-primary" style="width: 60%;"></div>
            </div>
        </div> --}}
    </div>
</div>