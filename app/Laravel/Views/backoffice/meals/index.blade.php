@extends('backoffice._layouts.main')

@section('content')
<div class="be-content">
        <div class="page-head">
            <h2 class="page-head-title">List of {{$page_title}}</h2>
            <ol class="breadcrumb page-head-nav">
                <li>
                    <a href="{{route('backoffice.dashboard')}}">Home</a>
                </li>
                <li class="active">All Records</li>
            </ol>
        </div>
        <div class="main-content container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    @include('backoffice._components.notification')
                    <div class="panel panel-default panel-table">
                        <div class="panel-heading">
                            {{$page_title}}
                            <div class="tools dropdown">
                                <a href="#" data-toggle="modal" data-target=".create"><span class="icon mdi mdi-plus"></span></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <table class="table table-striped table-hover table-fw-widget" id="table1">
                                <thead>
                                    <tr>
                                        <th style="width: 5%;">#</th>
                                        <th>Name</th>
                                        <th>Price</th>
                                        <th>Description</th>
                                        <th>Last Update</th>
                                        <th class="actions"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                	@foreach($data as $index => $info)
                                    <tr class="odd gradeX">
                                        <td>{{$index+1}}</td>
                                        <td>{{$info->name}}</td>
                                        <td>{{$info->price}}</td>
                                        <td>{{Str::limit($info->description,$limit = 30)}}</td>
                                        <td>{{Helper::date_format($info->updated_at,'F d, Y')}}</td>
                                        <td class="actions">
                                        	<div class="btn-group btn-hspace">
                                        		<button type="button" data-toggle="dropdown" class="btn btn-default dropdown-toggle">Action <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                                        		<ul role="menu" class="dropdown-menu pull-right">
                                                    <li>
                                                        <a href="#" data-toggle="modal" data-target=".view" 
                                                        data-image="{{asset($info->directory.'/'.$info->filename)}}"
                                                        data-category_id="{{$info->category_id}}"
                                                        data-name="{{$info->name}}" 
                                                        data-price="{{$info->price}}" 
                                                        data-description="{{$info->description}}" 
                                                        data-category="{{$info->category?$info->category->category_name:''}}" 
                                                        class="action-view"
                                                        >View</a>
                                                    </li>
                                                    <li>
                                                        <a href="#" data-toggle="modal" data-target=".edit" 
                                                        data-image="{{asset($info->directory.'/'.$info->filename)}}"
                                                        data-category_id="{{$info->category_id}}"
                                                        data-name="{{$info->name}}"
                                                        data-price="{{$info->price}}"
                                                        data-description="{{$info->description}}"
                                                        data-url="{{route('backoffice.'.$route_file.'.update',$info->id)}}" 
                                                        class="action-edit">Edit</a>
                                                    </li>
                                        			<li>
                                        				<a href="#" data-toggle="modal" data-url="{{route('backoffice.'.$route_file.'.destroy',[$info->id])}}" data-target="#delete" class="action-delete">Delete</a>
                                        			</li>
                                        		</ul>
                                        	</div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('page-modals')
<div id="delete" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <div class="text-danger"><span class="modal-main-icon mdi mdi-close-circle-o"></span></div>
                    <h3>Danger!</h3>
                    <p>This action can not be undone.<br>You are about to delete a record, this action can no longer be undone,<br> are you sure you want to proceed?</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
                <a type="button" class="btn btn-danger" id="btn-confirm-delete">Proceed</a>
            </div>
        </div>
    </div>
</div>

<div tabindex="-1" role="dialog" class="modal fade view">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
                <h4>View Meal</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <small>Category :</small>
                        <p><span class="meal-category"></span></p>
                        <small>Name :</small>
                        <p><span class="meal-name"></span></p>
                        <small>Price :</small>
                        <p><span class="meal-price"></span></p>
                        <small>Description :</small>
                        <p><span class="meal-description"></span></p>
                    </div>
                    <div class="col-md-6">
                        <small>Current Image :</small>
                        <div class="row">
                            <div class="col-md-12">
                                <img class="meal-image img img-thumbnail" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
            </div>
        </div>
    </div>
</div>

<div tabindex="-1" role="dialog" class="modal fade edit">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
                <h4>Edit Meal</h4>
            </div>
            {{ Form::open(['class' => "form-edit" ,'data-parsley-validate' => " " ,'novalidate' => " ", 'enctype' => "multipart/form-data"] ) }}
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <small>Menu Category</small>
                            {!!Form::Select('category_id',$categories,old('category_id'),['class' => "form-control input-sm meal-category-input" , 'parsley-trigger' => "change" , 'required' => ""])!!}
                        </div>
                        <br>
                        <div class="form-group">
                            <small>Meal Name</small>
                            <input name="name" type="text" class="form-control input-sm meal-name"  parsley-trigger="change" required="required">
                        </div>
                        <br>
                        <div class="form-group">
                            <small>Price</small>
                            <input name="price" type="text" class="form-control input-sm meal-price"  parsley-trigger="change" required="required">
                            <small class="text-info">Note: this field is on a text format</small>
                        </div>
                        <br>
                        <div class="form-group">
                            <small>Description</small>
                            <textarea name="description" class="form-control f-12 meal-description" rows="3" placeholder="Please enter some short description" parsley-trigger="change" required="required"></textarea>
                        </div>
                        <br>
                        <div class="form-group">
                            <small>Meal Thumbnail</small>
                            <input type="file" name="file" class="input-sm form-control has-error" value="" parsley-trigger="change" parsley-type="file">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <small>Current Image :</small>
                        <div class="row">
                            <div class="col-md-12">
                                <img class="meal-image img img-thumbnail" />
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            {{Form::close()}}
        </div>
    </div>
</div>

<div tabindex="-1" role="dialog" class="modal fade create">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
                <h4>
                    Create Meal
                </h4>
            </div>
            {{ Form::open(['route' => 'backoffice.'.$route_file.'.store','data-parsley-validate' => " " ,'novalidate' => " ", 'enctype' => "multipart/form-data"] ) }}
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <small>Menu Category</small>
                            {!!Form::Select('category_id',$categories,old('category_id'),['class' => "form-control input-sm" , 'parsley-trigger' => "change" , 'required' => ""])!!}
                        </div>
                        <br>
                        <div class="form-group">
                            <small>Meal Name</small>
                            <input name="name" type="text" class="form-control input-sm"  parsley-trigger="change" required="required">
                        </div>
                        <br>
                        <div class="form-group">
                            <small>Price</small>
                            <input name="price" type="text" class="form-control input-sm"  parsley-trigger="change" required="required">
                            <small class="text-info">Note: this field is on a text format</small>
                        </div>
                        <br>
                        <div class="form-group">
                            <small>Description</small>
                            <textarea name="description" class="form-control f-12" rows="3" placeholder="Please enter some short description" parsley-trigger="change" required="required"></textarea>
                        </div>
                        <br>
                        <div class="form-group">
                            <small>Meal Thumbnail</small>
                            <input type="file" name="file" class="input-sm form-control has-error" value="" parsley-trigger="change" parsley-type="file">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            {{Form::close()}}
        </div>
    </div>
</div>
@stop

@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/assets/lib/material-design-icons/css/material-design-iconic-font.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('backoffice/assets/lib/datatables/css/dataTables.bootstrap.min.css')}}"/>
<link rel="stylesheet" href="{{asset('backoffice/assets/css/style.css')}}" type="text/css"/>
<style type="text/css">
    .m-b-20{
        margin-bottom: 20px;
    }
    .parsley-errors-list.filled {
        padding: 5px 10px!important;
    }
    .input-sm{
        height: 30px!important;
        font-size: 12px!important;
    }
    .f-12{
        font-size: 12px!important;
    }
</style>
@stop

@section('page-scripts')
<script src="{{asset('backoffice/assets/lib/jquery/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/assets/js/main.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/assets/lib/bootstrap/dist/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/assets/lib/datatables/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/assets/lib/datatables/js/dataTables.bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/assets/lib/datatables/plugins/buttons/js/dataTables.buttons.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/assets/lib/datatables/plugins/buttons/js/buttons.html5.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/assets/lib/datatables/plugins/buttons/js/buttons.flash.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/assets/lib/datatables/plugins/buttons/js/buttons.print.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/assets/lib/datatables/plugins/buttons/js/buttons.colVis.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/assets/lib/datatables/plugins/buttons/js/buttons.bootstrap.js')}}" type="text/javascript"></script>
<script src="{{asset('backoffice/assets/lib/parsley/parsley.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function(){
        //initialize the javascript
        App.init();
        App.dataTables();
    });
  
    $(".action-delete").on("click",function(){
        var btn = $(this);
        $("#btn-confirm-delete").attr({"href" : btn.data('url')});
    });
  
    $(".action-edit").on("click",function(){
        var btn = $(this);
        $(".form-edit").attr({"action" : btn.data('url')});
        $(".meal-category-input").val(btn.data('category_id'));
        $(".meal-name").val(btn.data('name'));
        $(".meal-price").val(btn.data('price'));
        $(".meal-description").val(btn.data('description'));
        $(".meal-image").attr({"src" : btn.data('image')});
    });
  
    $(".action-view").on("click",function(){
        var btn = $(this);
        $(".meal-category").text(btn.data('category'));
        $(".meal-name").text(btn.data('name'));
        $(".meal-price").text(btn.data('price'));
        $(".meal-description").text(btn.data('description'));
        $(".meal-image").attr({"src" : btn.data('image')});
    });
</script>
@stop
