@extends('frontend._layouts.main')

@section('content')
<!--HOME START-->
        <div id="home" class="clearfix">
        </div><!--/home-->
        <!--HOME END-->


        <!--ABOUT START-->
        <div id="about" class="content">
          <div class="container">
               <div class="col-md-12">
                <div class="table-cell-box banner-box">
                  <h3 class="brand-title">About RHG</h3>
                  <p class="mb-50">{!!$about->content!!}</p>
                </div>
              </div>
                <!-- start commitment -->
            </div><!--/.container-fluid-->
        </div><!--/about-->

        <!--ABOUT START-->
        <div id="about" class="content">
          <div class="container-fluid">
                <div class="content-head">
                    <h3 class="content-title">Team</h3>
                </div>
                @foreach($team as $index => $value)
                 @if ($index % 2 == 0)
                 <div class="row table-box table-no-padding">
                     <div class="col-md-4 table-cell-box no-float">                     
                       <img src="{{asset($value->directory.'/'.$value->filename)}}" class="img-responsive">
                     </div><!--/.col-md-4-->
                       <div class="spacing40 clearfix"></div>
                     <div class="col-md-8 img-height table-cell-box no-float">
                       <div class="spacing80 clearfix"></div>
                       <div class="hero-box">
                             <h3 class="animated about-name" data-animated="fadeInUp" data-duration="1s">{{$value->name}}</h3>
                             <p class="position">{{$value->title}}</p>
                             <p class="animated" data-animated="fadeInUp" data-duration="1s" data-delay="0.5s" >
                               {{$value->description}}
                               <div class="btn-relativ mt-20">
                                 <a class="slider-btn" href="">Read More</a>
                               </div>
                             </p> 
                         </div><!--/.hero-box-->
                     </div><!--/.col-md-8-->
                 </div><!--/.row-->
                 @else
                 <div class="row table-box table-no-padding"> 
                       <div class="spacing40 clearfix"></div>
                             <div class="col-md-8 img-height table-cell-box no-float">
                               <div class="spacing80 clearfix"></div>
                               <div class="hero-box text-right">
                                     <h3 class="animated about-name" data-animated="fadeInUp" data-duration="1s">{{$value->name}}</h3>
                                     <p class="position">{{$value->title}}</p>
                                     <p class="animated" data-animated="fadeInUp" data-duration="1s" data-delay="0.5s" >{{$value->description}}</p> 
                                     <div class="btn-relative mt-20">
                                       <a class="slider-btn" href="#">Read More</a>
                                    </div>
                                 </div><!--/.hero-box-->
                             </div><!--/.col-md-8-->

                     <div class="col-md-4 table-cell-box no-float">
                       <img src="{{asset($value->directory.'/'.$value->filename)}}" class="img-responsive">
                       
                     </div><!--/.col-md-4-->
                 </div><!--/.row-->
                 @endif
                
                @endforeach               

                {{--

                <div class="row table-box table-no-padding"> 
                      <div class="spacing40 clearfix"></div>
                            <div class="col-md-8 img-height table-cell-box no-float">
                              <div class="spacing80 clearfix"></div>
                              <div class="hero-box text-right">
                                    <h3 class="animated about-name" data-animated="fadeInUp" data-duration="1s">{{$value->name}}</h3>
                                    <p class="position">{{$value->title}}</p>
                                    <p class="animated" data-animated="fadeInUp" data-duration="1s" data-delay="0.5s" >{{$value->description}}</p> 
                                    <div class="btn-relative mt-20">
                                      <a class="slider-btn" href="team.html">Read More</a>
                                   </div>
                                </div><!--/.hero-box-->
                            </div><!--/.col-md-8-->

                    <div class="col-md-4 table-cell-box no-float">
                      <img src="{{asset($value->directory.'/'.$value->filename)}}" class="img-responsive">
                      
                    </div><!--/.col-md-4-->
                </div><!--/.row-->
                    --}}

                
            </div><!--/.container-fluid-->
        </div><!--/about-->
        <!--ABOUT END-->

        {{--<div class="container">
            @foreach($others as $index => $value)
            <div class="row">
                <!-- <h3 class="mission-title"> {{$value->title}}</h3> -->
                <div class="col-md-12 img-height table-cell-box no-float">
                    
                    <p>{!!$value->content!!}
                    </p>
                </div><!--/.col-md-8-->
            </div>
            @endforeach
        </div>
            --}}

        


       

       

        <!--ABOUT END-->


@endsection

@section('page-styles')
 <title>About Us | Romlas Health Group</title>

@endsection

@section('page-metas')
@endsection