@extends('frontend._layouts.main')

@section('content') 
<div class="about-banner-area">
        <div class="container-fluid" style="width: 100% !important; padding: 0px !important;">
           <img src="{{asset($category->directory.'/'.$category->filename)}}" style="width: 100% !important; padding: 0px !important;">
        </div>
    </div>

    <!-- shop-top-area-start -->
    <div class="" style="background-image: url('{{asset('frontend/img/bg-ital.jpg')}}'); padding: 50px 0 50px !important;">
        <div class="container-fluid width-eighty">
            
            @include('frontend._components.menu_list')


            <div class="tab-content menu-tab-content">
                <div class="tab-pane fade in active" id="antipasto">
                    <h1 class="menu-main-title">{{$category->category_name}}</h1>
                    <div class="row">
                        @foreach($meals as $index => $info)
                        <div class="col-md-4 max-menu-height">
                            <div class="tab-pane fade in active" id="home">
                                <img class="img-responsive menu-img" src="{{asset($info->directory.'/'.$info->filename)}}">
                                <h4 class="menu-title">{{$info->name}}</h4>
                                <p class="menu-description">{{$info->description}}</p>
                                <p class="menu-price">{{$info->price}}</p>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                @foreach($children as $index => $child)
                <div class="tab-pane fade in active" id="antipasto">
                    <h1 class="menu-main-title">{{$child->category_name}}</h1>
                    <div class="row">
                        @foreach($child->meals as $index => $info)
                        <div class="col-md-4">
                            <div class="tab-pane fade in active" id="home">
                                <img class="img-responsive menu-img" src="{{asset($info->directory.'/'.$info->filename)}}">
                                <h4 class="menu-title">{{$info->name}}</h4>
                                <p class="menu-description">{{$info->description}}</p>
                                <p class="menu-price">{{$info->price}}</p>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div><!-- shop-top-area-end -->
@endsection

@section('page-metas')
<title>{{Str::title($category->category_name)}} | Italianni's Restaurant Philippines</title>
@endsection