<!DOCTYPE html>
<html lang="en">

<head> 
     @include('frontend._components.metas')
     @yield('page-metas')

     @include('frontend._includes.styles')
     @yield('page-styles') 
</head>

    <body class="multi-page">
     @include('frontend._components.header')
    
        <!-- Preloader -->
        <div id="preloader">
            <div id="status">
                <div class="spinner">
                  <div class="double-bounce1"></div>
                  <div class="double-bounce2"></div>
                </div>
            </div><!--status-->
        </div><!--/preloader-->
        
        @yield('content')
        
        @include('frontend._components.footer')
        <!-- Scripts -->

        @include('frontend._includes.scripts')
        @yield('page-scripts')

    </body>
</html>