<!DOCTYPE html>
<html lang="en">

<head> 
   @include('frontend._components.metas')
   @yield('page-metas')

   @include('frontend._includes.styles')
   @yield('page-styles') 
</head>

<body class="multi-page">
   @include('frontend._components.header')
   
   <!-- Preloader -->
   <div id="preloader">
    <div id="status">
        <div class="spinner">
          <div class="double-bounce1"></div>
          <div class="double-bounce2"></div>
      </div>
  </div><!--status-->
</div><!--/preloader-->

@yield('content')

<footer class="footer clearfix">
    <div class="container-fluid">
        <div class="footer-wigdet clearfix">
            <div class="row">
                <div class="col-md-4 ">
                    <div class="widget text-widget">
                        @if($web_info->filename)
                        <img alt="logo" src="{{asset($web_info->directory.'/'.$web_info->filename)}}">
                        @else
                        <img alt="logo" src="{{asset('frontend/images/logo-white.png')}}">
                        @endif
                        <p>{{$web_info->description ? : "No description yet"}}</p>
                        <div class="widget text-widget mt-50">
                              
                            <p>{{$web_info->address ? : "No address yet"}}</p>
                            <p><i class="fa fa-phone"></i> {{$web_info->phone ?: "No contact number yet"}}</p>
                            <p><i class="fa fa-envelope"></i> <a href="#">{{$web_info->email ? : "No email yet"}}</a></p>
                        </div><!--/.recent-post-->
                    </div><!--/.widget-->
                </div><!--/.col-md-4--> 
                
                
            </div><!--/.row-->
            <div class="spacing40 clearboth"></div>
        </div><!--/.footer-widget-->
        
        <div class="row">
            <div class="col-md-4">
                <p>Romlas Health Group | Copyright  &copy;2018</p>
            </div><!--/.col-md-6-->

            <div class="col-md-4">
                <p class="text-justify">
                    <a href="#" class="pr-25">Privacy Policy</a>
                    <a href="#" class="pr-25">Press</a>
                    <a href="#" class="pr-25">Careers</a>
                </p>
            </div><!--/.col-md-6-->
            
            <div class="col-md-4 footer-box">
                <ul class="footer-icon">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                </ul><!--/.team-icon-->
            </div><!--/.footer-box-->
        </div><!--/.row-->          
    </div><!--/.container-fluid-->
</footer><!--/.footer--> 

<!-- Scripts -->

@include('frontend._includes.scripts')
@yield('page-scripts')
<script src="http://maps.google.com/maps/api/js?key={{  env("GOOGLE_MAP_KEY") }}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('frontend/dimp/scripts/infobox.min.js')}}"></script>
<script type="text/javascript" src="{{asset('frontend/dimp/scripts/markerclusterer.js')}}"></script>
<script type="text/javascript" src="{{asset('frontend/dimp/scripts/maps.js')}}"></script>  

</body>
</html>