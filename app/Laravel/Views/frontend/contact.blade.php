  @extends('frontend._layouts.contact')

  @section('content')
  <!--HOME START-->
  <div id="home" class="clearfix">
  </div><!--/home-->
  <!--HOME END-->

  <!--BANNER START-->
  <div class="banner mt-30">
    <div class="container-fluid">
      <div class="banner-parent clearfix">
        <div class="img-bg clearfix banner-img animated" data-animated="puffIn" data-duration="2s"
        data-background="{{asset($contact->directory.'/'.$contact->filename)}}" data-stellar-background-ratio="0.8"></div>

        <div class="work-slider-box work-box-two">
          <div class="slider-content">
            <!--HERO CLEAN START-->
            <div class="hero-clean hero-work">
              <div class="hero-box">
                <h3 class="animated contact-title" data-animated="vanishIn"  data-duration="1s" data-delay="0.5s">Contact Us</h3>
                <p class="animated" data-animated="vanishIn" data-duration="1s" data-delay="0.9s" > 
                  {{$contact->title ? : "No content yet"}}
                </p>
              </div>
            </div><!--/hero-clean-->
            <!--HERO CLEAN END-->
          </div><!--slider-content-->
        </div><!--/.work-slider-box-->

        <div class="banner-mask"></div>
      </div><!--/.banner-parent-->
    </div><!--/.container-fluid-->
  </div><!--/.banner-->
  <!--BANNER END-->

  <!--CONTACT START-->
  <div id="contact" class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-6">
          <p>{!!$contact->content!!}</p>
        </div>
      </div><!--/.col-md-6-->

      <div class="row">
        <div class="col-md-4">

          <ul class="contact-list">
            <div class="row pb-5">
              <div class="col-md-2 col-sm-2 col-xs-2">
                <i class="fa fa-home"></i>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                {{$info->phone ? : "No content yet"}}
              </div>
            </div>
            <div class="row pb-5">
              <div class="col-md-2 col-sm-2 col-xs-2">
                <i class="fa fa-phone"></i>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                {{$info->phone ? : "No content yet"}}
              </div>
            </div>
            <div class="row pb-5">
              <div class="col-md-2 col-sm-2 col-xs-2">
                <i class="fa fa-envelope"></i>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                {{$info->email ? : "No content yet"}}
              </div>
            </div>
            <div class="row pb-5">
              <div class="col-md-2 col-sm-2 col-xs-2">
                <i class="fa fa-clock-o"></i>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                {{$info->operations ? : "No content yet"}}
              </div>
            </div>

          </ul>
        </div>

        <div class="col-md-2"></div>

        <div class="col-md-6">

          <div id="form-wrapper">
            <div id="form-inner">
              <div id="ErrResults"><!-- retrive Error Here --></div>
              <div id="MainResult"><!-- retrive response Here --></div>
              <div id="MainContent">

               <form id="MyContactForm" action="{{ route('frontend.send') }}" name="MyContactForm" method="post">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <p class="name">
                 <input type="text" name="name" id="name" placeholder="Your Name ...">
               </p>
               <p>
                 <input type="email" name="email" id="email" placeholder="Your Email ...">
                 <label for="email" id="emailLb">
                 </label> 
               </p>
               <p> 
                 <input type="text" name="phone" id="phone" placeholder="Your Telephone ...">
               </p>
               <p class="textarea">
                 <textarea name="message" id="message" placeholder="Your Message ..." rows="5"></textarea>
               </p>
               <div class="clearfix"></div>
               <button type="submit" class="black-btn contact-btn contact-btn">Send Message</button>
             </form>
           </div><!--MainContent-->
         </div><!--form-inner-->
       </div><!--form-wrapper-->
       <div class="spacing40 clearfix"></div>
     </div><!--/.col-md-6-->

   </div><!--/.row-->
   <!-- Location -->
   <div id="listing-location" class="listing-section">
     <h3 class="listing-desc-headline margin-top-60 margin-bottom-30">Location</h3>

     <div id="singleListingMap-container">
       <div id="singleListingMap" data-latitude="{{$web_info->geo_lat}}" data-longitude="{{$web_info->geo_long}}" data-map-icon="im im-icon-File-ClipboardTextImage"></div>
       <a href="#" id="streetView">Street View</a>
     </div>
   </div>

  </div><!--/.container-fluid-->

  </div><!--/contact-->
  <!--CONTACT END-->

 

  @endsection

  @section('page-metas')
 <title>Contact Us</title>

  @endsection