<footer class="footer clearfix">
            <div class="container-fluid">
                <div class="footer-wigdet clearfix">
                    <div class="row">
                        <div class="col-md-4 ">
                            <div class="widget text-widget">
                                @if($web_info->filename)
                                <img alt="logo" src="{{asset($web_info->directory.'/'.$web_info->filename)}}">
                                @else
                                <img alt="logo" src="{{asset('frontend/images/logo-white.png')}}">
                                @endif
                                <p>{{$web_info->description ? : "No description yet"}}</p>
                                <div class="widget text-widget mt-50">
                                    <p><i class="fa fa-home"></i> {{$web_info->address ? : "No address yet"}}</p>
                                    <p><i class="fa fa-phone"></i> {{$web_info->phone ?: "No contact number yet"}}</p>
                                    <p><i class="fa fa-envelope"></i> <a href="#">{{$web_info->email ? : "No email yet"}}</a></p>
                                </div><!--/.recent-post-->
                            </div><!--/.widget-->
                        </div><!--/.col-md-4--> 
                        
                        
                        <div class="col-md-offset-2 col-md-6">
                            <div id="form-wrapper">
                                <div id="form-inner">
                                    <div id="ErrResults"><!-- retrive Error Here --></div>
                                    <div id="MainResult"><!-- retrive response Here --></div>
                                    <div id="MainContent">
                                     <form id="" action="{{ route('frontend.send') }}" name="MyContactForm" method="post" class="form-footer">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                         <p class="name" style="">
                                            <input type="text" name="name" id="name" placeholder="Your Name ..." class="mb-10">
                                         </p>
                                         <p>
                                            <input type="email" name="email" id="email" placeholder="Your Email ..." class="mb-10"> 
                                         </p>
                                         <p> 
                                            <input type="text" name="phone" id="phone" placeholder="Your Telephone ..." class="mb-10">
                                         </p>
                                         <p class="textarea">
                                            <textarea name="message" id="message" placeholder="Your Message ..." rows="5" class="mb-10"></textarea>
                                         </p>
                                         <div class="clearfix"></div>
                                         <button type="submit" class="black-btn contact-btn contact-btn">Send Message</button>
                                     </form>
                                 </div><!--MainContent-->
                             </div><!--form-inner-->
                         </div><!--form-wrapper-->
                         <div class="spacing40 clearfix"></div>
                     </div><!--/.col-md-6-->
                     
                       
                    </div><!--/.row-->
                    <div class="spacing40 clearboth"></div>
                </div><!--/.footer-widget-->
                
                <div class="row">
                    <div class="col-md-4">
                        <p>Romlas Health Group | Copyright  &copy;2018</p>
                    </div><!--/.col-md-6-->

                    <div class="col-md-4 text-center">
                        <p class="text-justify">
                            <a href="#" class="pr-25">Privacy Policy</a>
                            <a href="#" class="pr-25">Press</a>
                            <a href="{{route('frontend.careers.index')}}" class="pr-25">Careers</a>
                        </p>
                    </div><!--/.col-md-6-->
                    
                    <div class="col-md-4 footer-box">
                        <ul class="footer-icon">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul><!--/.team-icon-->
                    </div><!--/.footer-box-->
                </div><!--/.row-->          
            </div><!--/.container-fluid-->
        </footer><!--/.footer--> 
