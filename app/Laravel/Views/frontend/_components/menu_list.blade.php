<div class="hidden-sm hidden-xs">
    <div class="text-center">
        <ul class="nav nav-tabs menu-list">
            @foreach($categories as $index => $info)
            <a class="menu-items" href="{{route('frontend.menu_category',['id' => $info->id , 'category' => $info->category_name])}}">{{$info->category_name}}</a>
            @endforeach
            
        </ul>
    </div>
</div>

<div class="hidden-md hidden-lg">
    <div class="text-center">
        <ul class="nav nav-tabs menu-list">
            @foreach($categories as $index => $info)
            <li><a class="menu-items" href="{{route('frontend.menu_category',['id' => $info->id , 'category' => $info->category_name])}}">{{$info->category_name}}</a></li>
            @endforeach
        </ul>
    </div>
</div>