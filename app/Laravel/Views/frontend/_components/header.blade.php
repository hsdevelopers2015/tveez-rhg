 <style type="text/css" media="screen">
    .active a {
        /*background-color: grey;
        opacity: 21;*/
        color:black !important;;
    } 
 </style>   
<nav class="header home-section clearfix ">
        <div class="top-head">
            <div class="container-fluid">
            </div><!--/.container-fluid-->
        </div><!--/.top-head-->
        <div class="nav-head white-nav">
            <div class="logo">
                <a href="{{route('frontend.index')}}">
                    <img class="logo1" alt="logo" src="{{asset('frontend/images/logo.png')}}">
                </a>
            </div>
            <div class="container-fluid nav-box">
                <div class="for-sticky">
                    <div class="menu-box hidden-xs hidden-sm">
                        <ul class="navigation myfont">                                        
                            <li class="{{ active_class(if_route(['frontend.index']), 'active') }}">
                                <a href="{{route('frontend.index')}}">Home</a>
                            </li>
                            <li class="{{ active_class(if_route(['frontend.about']), 'active') }}">
                                <a href="{{route('frontend.about')}}">About Us</a>
                            </li>
                            <li class="{{ active_class(if_route(['frontend.brand']), 'active') }}">
                                <a href="{{route('frontend.brand')}}">Brands and Affiliates</a>
                            </li>
                            <li class="{{ active_class(if_route(['frontend.partners']), 'active') }}">
                                <a href="{{route('frontend.partners')}}">Global Partners</a>
                            </li>
                            <li class="{{ active_class(if_route(['frontend.contact']), 'active') }}">
                                <a href="{{route('frontend.contact')}}">Contact Us</a>
                            </li>
                            <li class="{{ active_class(if_route(['frontend.careers']), 'active') }}">
                                <a href="{{route('frontend.careers.index')}}">Careers</a>
                            </li>
                        </ul>
                    </div><!--/.menu-box-->
                    <div class="box-mobile hidden-lg hidden-md">
                        <div class="menu-btn" data-toggle="collapse" data-target=".nav-collapse">
                            <span class="fa fa-bars"></span>
                        </div>
                        <ul class="nav-collapse mobile-menu hidden-lg hidden-md"></ul>
                    </div><!--/.box-mobile-->   
                </div><!--/.for-sticky-->
            </div><!--/.container-fluid-->
        </div><!--/.nav-head-->
</nav><!--/.header-->