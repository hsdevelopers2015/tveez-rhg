@extends('frontend._layouts.main')

@section('content')
<!--HOME START-->
        <div id="home" class="clearfix">
        </div><!--/home-->
        <!--HOME END-->
        
        <div class="content blog-wrapper">  
            <div class="container-fluid clearfix">
                <div class="row blog-mason clearfix">
                    <div class="col-md-12">
                        <div class="table-cell-box banner-box">
                            <h3 class="brand-title">Careers</h3>
                            <p class="mb-50">Lorem ipsum dolor sit aemer erejhre srd errejri eruya pirw sere. Lofme ipsum dolor.</p>
                        </div>
                    </div> 
                    <!--ABOUT START-->

                            <!-- Mission -->
                            <div class="row table-box table-no-padding">
                               <div class="col-md-4 table-cell-box no-float">
                                  <div class="box-relative work-img-box"  data-stellar-ratio="1.2">
                                        <div class="ani-width" data-delay="0.4s">
                                        
                                            <div class="about-box"></div>
                                            <div class="port-img width-img big-bg img-bg" data-background="{{asset($career->directory.'/'.$career->filename)}}"></div>
                                            <div class="img-mask"></div>
                                            
                                        </div><!--.port-item-->
                                    </div>
                                </div><!--/.col-md-4-->                      
                                <div class="col-md-8 img-height table-cell-box no-float">
                                  <div class="spacing80 clearfix"></div>
                                  <div class="hero-box">
                                        <h3 class="animated" data-animated="fadeInUp" data-duration="1s">{{$career->title}}</h3>
                                    </div><!--/.hero-box-->                                    
                                    <div class="spacing40 clearfix"></div>                                    
                                    <p>{!!$career->description!!} </p>
                                    <button type="submit" class="black-btn contact-btn contact-btn">Apply Now</button>                   
                                    <div class="spacing40 clearfix"></div>
                                </div><!--/.col-md-8-->
                            </div><!--/.row--> 
                </div><!--/.row-->
            </div><!--/.container-->
        </div><!--/.blog-wrapper-->


@endsection

@section('page-styles')

@endsection

@section('page-metas')
@endsection