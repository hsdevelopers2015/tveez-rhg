@extends('frontend._layouts.main')

@section('content')
<!--HOME START-->
        <div id="home" class="clearfix">
        </div><!--/home-->
        <!--HOME END-->
        
        <div class="content blog-wrapper">  
            <div class="container-fluid clearfix">
                <div class="row blog-mason clearfix">
                    <div class="col-md-12">
                        <div class="table-cell-box banner-box">
                            <h3 class="brand-title">Careers</h3>
                            <p class="mb-50">{!!$sub_content->content!!}</p>
                        </div>
                    </div>
                    @foreach($careers as $index => $value)
                    <div class="col-md-4">
                        <!--BLOG POST START-->
                        <article class="blog-post">    
                             <img src="{{asset($value->directory.'/'.$value->filename)}}" alt="blog">
                             <div class="spacing10 clearfix"></div>
                             <a href="#"><h2 class="career-title">{{$value->title}}</h2></a>
                             <div class="spacing40 clearfix"></div>
                            <p>{!!str_limit(strip_tags($value->description), 100)!!}</p>

                             <div class="spacing40 clearfix"></div>
                             <a class="black-btn" href="{{route('frontend.careers.show',$value->id)}}">Read More</a>
                             <div class="spacing40 clearfix"></div>
                             <div class="border-post clearfix"></div>
                             <div class="clearboth spacing40"></div>
                        </article><!--/.blog-post-->
                        <!--BLOG POST END-->
                    </div><!--/.col-md-4-->
                    @endforeach

                    

                </div><!--/.row-->
            </div><!--/.container-->
        </div><!--/.blog-wrapper-->


@endsection

@section('page-styles')

@endsection

@section('page-metas')
 <title>Careers</title>
@endsection