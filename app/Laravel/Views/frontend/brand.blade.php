@extends('frontend._layouts.main')

@section('content')
<!--HOME START-->
        <div id="home" class="clearfix">
        </div><!--/home-->
        <!--HOME END-->
        
        <div class="content blog-wrapper">  
            <div class="container-fluid clearfix">
                <div class="row blog-mason clearfix">
                    <div class="col-md-12">
                        <div class="table-cell-box banner-box">
                            <h3 class="brand-title">Our Brand and Affiliates</h3>
                            <p class="mb-50">Lorem ipsum dolor sit aemer erejhre srd errejri eruya pirw sere. Lofme ipsum dolor.</p>
                        </div>
                    </div>
                    @foreach($brands as $index => $value)
                    <div class="col-md-4">
                        <!--BLOG POST START-->
                        <article class="blog-post">                             
                             
                             <img src="{{asset($value->directory.'/'.$value->filename)}}" alt="blog">
                             <img src="{{asset($value->directory_logo.'/'.$value->filename_logo)}}" class="img-responsive">
                             <!-- <a href="#"><h2 class="blog-title">{{$value->title}}</h2></a> -->
                             
                             <div class="clearfix text-justify"></div>
                            <p>{!!str_limit(strip_tags($value->description), 100)!!}</p>
                             
                             <a class="black-btn" href="{{$value->url}}" target="_blank">Visit Site</a>
                        
                             <div class="spacing40 clearfix"></div>
                             <div class="border-post clearfix"></div>
                             <div class="clearboth spacing40"></div>
                        </article><!--/.blog-post-->
                        <!--BLOG POST END-->
                    </div><!--/.col-md-4-->

                    @endforeach

                </div><!--/.row-->
            </div><!--/.container-->
        </div><!--/.blog-wrapper-->



@endsection

@section('page-styles')

@endsection

@section('page-metas')
 <title>Our Brands and Affiliates</title>
@endsection