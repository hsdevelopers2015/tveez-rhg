@extends('frontend._layouts.main')

@section('content')
<!--HOME START-->
        <div id="home" class="clearfix">
        </div><!--/home-->
        <!--HOME END-->

         <!--WORKS START-->
        <div id="works" class="content">
            <div class="container-fluid">
                <div class="col-md-12">
                        <div class="table-cell-box banner-box">
                            <h3 class="brand-title">Our Global Partners</h3>
                            <p class="mb-50">Lorem ipsum dolor sit aemer erejhre srd errejri eruya pirw sere. Lofme ipsum dolor.</p>
                        </div>
                    </div>
                    @foreach($partners as $index => $value)
                    <div class="col-md-4">
                        <a href="{{$value->link}}" target="_blank">
                            <img src="{{asset($value->directory.'/'.$value->filename)}}" class="animated img-responsive" data-animated="fadeInUp" data-duration="1s">
                            <div class="spacing40 clearfix text-justify"></div>
                            <p class="text-justify">{!!$value->content!!}</p>
                        </a>
                  </div>
                    @endforeach
                                    
                </div><!--/.portfolio-body-->
                
                
            </div>
        </div><!--/works-->
        <!--WORKS END-->
        

@endsection

@section('page-styles')

@endsection

@section('page-metas')
 <title>Our Global Partners</title>

@endsection