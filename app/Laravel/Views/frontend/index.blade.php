@extends('frontend._layouts.main')

@section('content')
<!--HOME START-->
        <div id="home" class="clearfix">
        </div><!--/home-->
        <!--HOME END-->
        
        <!--HOME SLIDER START-->
        <div class="home-slider ani-slider slider container-fluid" data-slick='{"autoplaySpeed": 5000}'>

            @foreach($sliders as $index => $value)
            <div class="slide">
                <div class="slider-mask" data-animation="slideLeftReturn" data-delay="0.1s"></div>
                <div class="slider-img-bg" data-animation="vanishIn" data-delay="0.2s" data-animation-duration="0.7s" data-background="{{asset($value->directory.'/'.$value->filename)}}"></div>
                <div class="slider-box container-fluid">
                    <div class="row">
                        <div class="col-md-6 slider-content">
                            <h3 class="slider-title" data-animation="fadeInDownBig" data-delay="0.8s">{{$value->title}}</h3>
                            <p class="slider-text" data-animation="fadeInUp" data-delay="1s">
                                {{$value->description}}
                            </p>
                            
                            <div class="btn-relative" data-animation="swashIn" data-delay="1.5s" data-animation-duration="1s">
                                <a class="slider-btn" href="#">Start your health journey now</a>
                            </div><!--/.btn-relative-->
                        </div><!--/.col-md-6-->
                    </div><!--/.row-->
                </div><!--/.slider-box-->
            </div><!--/.slide-->
            @endforeach
            
            
          
        
        </div><!--/.slider-->
        <!--HOME SLIDER END-->
           
        <!--WORKS START-->
        <div id="works" class="content">
            <div class="container-fluid">
                <div class="content-head">
                    <h3 class="content-title" style="font-family: 'avenir';">Our Brands</h3>
                    <p class="sub-content">{!!$brand_subcontent->content!!}<p>
                </div>
                <!-- <ul class="port-filter animated" data-animated="fadeInUp">
                    <li><a class="active" href="#" data-filter="*">All</a></li>
                    <li><a href="#" data-filter=".design">Web Design</a></li>
                    <li><a href="#" data-filter=".development">Web Development</a></li>
                    <li><a href="#" data-filter=".photography">Photography</a></li>
                </ul> -->
                <div class="portfolio-body  clearfix">
                    <!-- @foreach($brands as $index => $value)
                    <div class="col-md-4 brand-container">
                      <img src="{{asset($value->directory_logo.'/'.$value->filename_logo)}}" alt="Avatar" class="image">
                        
                    </div>
                    @endforeach   -->
                    <div class="container">
                      <section class="customer-logos slider">
                        @foreach($brands as $index => $value)
                        <div class="col-md-4 brand-container slide" style="width: 100% !important;">
                          <img src="{{asset($value->directory_logo.'/'.$value->filename_logo)}}" alt="Avatar" class="image">

                      </div>
                      @endforeach 
                      <!-- <div class="slide"><img src="https://image.freepik.com/free-vector/luxury-letter-e-logo-design_1017-8903.jpg"></div>
                      <div class="slide"><img src="https://image.freepik.com/free-vector/3d-box-logo_1103-876.jpg"></div>
                      <div class="slide"><img src="https://image.freepik.com/free-vector/blue-tech-logo_1103-822.jpg"></div>
                      <div class="slide"><img src="https://image.freepik.com/free-vector/colors-curl-logo-template_23-2147536125.jpg"></div>
                      <div class="slide"><img src="https://image.freepik.com/free-vector/abstract-cross-logo_23-2147536124.jpg"></div>
                      <div class="slide"><img src="https://image.freepik.com/free-vector/football-logo-background_1195-244.jpg"></div>
                      <div class="slide"><img src="https://image.freepik.com/free-vector/background-of-spots-halftone_1035-3847.jpg"></div>
                      <div class="slide"><img src="https://image.freepik.com/free-vector/retro-label-on-rustic-background_82147503374.jpg"></div> -->
                  </section>
              </div>
                </div><!--/.portfolio-body-->




            </div>


        </div><!--/works-->
        <!--WORKS END-->


        <!--ABOUT START-->
        <div id="about" class="content">
          <div class="container-fluid">
                <div class="content-head">
                    <h3 class="content-title">Directors</h3>
                </div>
                @foreach($team as $index => $value)
                 @if ($index % 2 == 0)
                 <div class="row table-box table-no-padding">
                     <div class="col-md-4 table-cell-box no-float">                     
                       <img src="{{asset($value->directory.'/'.$value->filename)}}" class="img-responsive">
                     </div><!--/.col-md-4-->
                       <div class="spacing40 clearfix"></div>
                     <div class="col-md-8 img-height table-cell-box no-float">
                       <div class="spacing80 clearfix"></div>
                       <div class="hero-box">
                             <h3 class="animated about-name" data-animated="fadeInUp" data-duration="1s">{{$value->name}}</h3>
                             <p class="position">{{$value->title}}</p>
                             <p class="animated" data-animated="fadeInUp" data-duration="1s" data-delay="0.5s" >
                               {{$value->description}}
                               <div class="btn-relativ mt-20">
                                 <a class="slider-btn" href="">Read More</a>
                               </div>
                             </p> 
                         </div><!--/.hero-box-->
                     </div><!--/.col-md-8-->
                 </div><!--/.row-->
                 @else
                 <div class="row table-box table-no-padding"> 
                       <div class="spacing40 clearfix"></div>
                             <div class="col-md-8 img-height table-cell-box no-float">
                               <div class="spacing80 clearfix"></div>
                               <div class="hero-box text-right">
                                     <h3 class="animated about-name" data-animated="fadeInUp" data-duration="1s">{{$value->name}}</h3>
                                     <p class="position">{{$value->title}}</p>
                                     <p class="animated" data-animated="fadeInUp" data-duration="1s" data-delay="0.5s" >{{$value->description}}</p> 
                                     <div class="btn-relative mt-20">
                                       <a class="slider-btn" href="#">Read More</a>
                                    </div>
                                 </div><!--/.hero-box-->
                             </div><!--/.col-md-8-->

                     <div class="col-md-4 table-cell-box no-float">
                       <img src="{{asset($value->directory.'/'.$value->filename)}}" class="img-responsive">
                       
                     </div><!--/.col-md-4-->
                 </div><!--/.row-->
                 @endif
                
                @endforeach               

                {{--

                <div class="row table-box table-no-padding"> 
                      <div class="spacing40 clearfix"></div>
                            <div class="col-md-8 img-height table-cell-box no-float">
                              <div class="spacing80 clearfix"></div>
                              <div class="hero-box text-right">
                                    <h3 class="animated about-name" data-animated="fadeInUp" data-duration="1s">{{$value->name}}</h3>
                                    <p class="position">{{$value->title}}</p>
                                    <p class="animated" data-animated="fadeInUp" data-duration="1s" data-delay="0.5s" >{{$value->description}}</p> 
                                    <div class="btn-relative mt-20">
                                      <a class="slider-btn" href="team.html">Read More</a>
                                   </div>
                                </div><!--/.hero-box-->
                            </div><!--/.col-md-8-->

                    <div class="col-md-4 table-cell-box no-float">
                      <img src="{{asset($value->directory.'/'.$value->filename)}}" class="img-responsive">
                      
                    </div><!--/.col-md-4-->
                </div><!--/.row-->
                    --}}

                
            </div><!--/.container-fluid-->
        </div><!--/about-->
        <!--ABOUT END-->

        <!--WORKS START-->
        <div id="works" class="content">
            <div class="container-fluid">
                <div class="content-head">
                    <h3 class="content-title">Our Global Partners</h3>
                </div>
                <div class="portfolio-body  clearfix">
                    @foreach($partners as $index => $value)                   
                    <div class="col-md-4">
                    <a href="">
                      <img src="{{asset($value->directory.'/'.$value->filename)}}" class="animated img-responsive" data-animated="fadeInUp" data-duration="1s">
                    </a>
                    </div>
                    @endforeach
                  
                </div><!--/.portfolio-body-->
                
                
            </div>
        </div><!--/works-->
        <!--WORKS END-->
        
        <!--BANNER START-->
        
        <div class="banner banner-margin-bottom">
            <div class="container-fluid">
                <div class="banner-parent clearfix">
                    <div class="img-bg clearfix banner-img animated" data-animated="puffIn" data-duration="2s"
                data-background="{{asset($contact->directory.'/'.$contact->filename)}}" data-stellar-background-ratio="0.8"></div>
                    <div class="table-box">
                        <div class="table-cell-box banner-box">
                            <h3 class="banner-title">{{$contact->title ? : "No content yet"}}</h3>
                            <p>{!!$contact->content ? : "No content yet"!!} </p>
                        </div><!--/.table-cell-box-->
                        
                        <div class="table-cell-box banner-btn-box">
                            <a href="{{asset('frontend.contact')}}" class="banner-btn">Contact Us</a>
                        </div><!--/.table-cell-box-->
                        
                    </div><!--/.table-box-->
                    <div class="banner-mask"></div>
                </div><!--/.banner-parent-->
            </div><!--/.container-fluid-->
        </div><!--/.banner-->
        <!--BANNER END-->
@endsection

@section('page-styles')



@endsection

@section('page-metas')
 <title>Romlas Health Group</title>
@endsection

@section('page-scripts')
<script type="text/javascript">
    $(document).ready(function(){
    $('.customer-logos').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 1500,
        arrows: false,
        dots: false,
        pauseOnHover: false,
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 4
            }
        }, {
            breakpoint: 520,
            settings: {
                slidesToShow: 3
            }
        }]
    });
});
</script>
@endsection